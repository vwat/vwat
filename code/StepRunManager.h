/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STEPRUNMANAGER_H_
#define __STEPRUNMANAGER_H_

#include "../code/RunManager.h"
#include "TGraph.h"
#include "baseline_processing/BaselineProcessor.h"
#include "filtering/FilterProcessor.h"
#include "pulse_analysis/PulseAnalysisProcessor.h"
#include "pulse_finding/PulseFindingProcessor.h"

/**
 * Simple root plotting utilities for plotting the waveforms and pulses
 */
namespace RootHelper {
extern void drawPulses(std::vector<Pulse> *pulses);
extern TF1 *getFitFunction(std::vector<Pulse> *pulses, Waveform *wf,
                           TF1 *fitFunction);
extern TGraph *getPulseGraph(std::vector<Pulse> *pulses);
} // namespace RootHelper

/**
 * An implementation of RunManager that allows for events to be graphed
 * individually. The results of the analysis are not saved to file See
 * EventStepperViewer.C for an example of implementation.
 */
class StepRunManager : public RunManager {
public:
  StepRunManager(int eventID = 0);
  void start();
  bool next();

  void processEvent();
  void pulseFinding();
  void pulseAnalysis();
  TF1 *getFitFunction(Pulse &p, int chID);
  void setFitFunction(std::vector<Pulse> *pulses, Waveform *wf,
                      TF1 *fitFunction, int chID);

  std::vector<Pulse> *getPulse(int chID);
  Waveform *getWaveform(int chID);
  Channel *getChannel(int chID);
  TH1D *getFilter(int ch);
  inline int getEventID() { return currEvent->getID(); };
  inline int getEventTriggerTime() { return currEvent->getTriggerTime(); }

protected:
  int eventID;
  int chID;
  std::unique_ptr<Reader> reader;
  FilterProcessor flProcessor;
  BaselineProcessor bsProcessor;
  PulseFindingProcessor pfProcessor;
  PulseAnalysisProcessor paProcessor;
  std::shared_ptr<Event> currEvent;
  int currentPulseIndex;
};

#endif // __STEPRUNMANAGER_H_