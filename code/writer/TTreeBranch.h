/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTREEBRANCH_H
#define TTREEBRANCH_H

#include "../data_structures/Channel.h"
#include "TClonesArray.h"
#include "TObject.h"

/**
 * Represents a branch in a TTree.
 * The data from each channel is represented by a TTreeBranch.
 */
class TTreeBranch : public TObject {
public:
  /**
   * TTreeBranch constructor.
   * Initializes the member variables to empty/zero.
   */
  TTreeBranch() : pulse(new TClonesArray("Pulse", 100)), npulse(0) {}
  virtual ~TTreeBranch();
  bool updateBranch(int indexEvent, Channel &ch);

  ClassDef(TTreeBranch, 1);

private:
  TClonesArray *pulse; /*!< Pulses from this Event and Channel. */
  Int_t npulse;        /*!< Number of Pulses. */
};

#endif // TTREEBRANCH_H