/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PulseWriteProcessor.h"

/**
 * PulseWriteProcessor constructor.
 *
 * Takes ChannelConfigs (one per channel) and maps their contents to pair of
 * vectors. When adding new a #Pulse writer class, register it in
 * this method along with a name that can be used in the JSON config file to
 * select it.
 *
 * If using an EventLevelProcessor that requires a specific event type, add a
 * mapping to the correct TTreeWriter derived class.
 */
PulseWriteProcessor::PulseWriteProcessor(
    std::vector<ChannelConfig> channelConfigs, std::shared_ptr<Event> event,
    DataSource dataSource)
    : mEvent(event) {
  for (auto channelCfg : channelConfigs) {
    IPulseWritingStrategy *ws;
    auto wsName = channelCfg.writingStrategy;

    if (wsName == "NTupleWriter") {
      ws = new NTupleWriter(channelCfg.vars, dataSource);
      sharedWriter = false;
    } else if (wsName == "N/A") {
      ws = nullptr;
    } else if (wsName == "TTreeWriter") {
      if (strategies.empty()) {
        // Map specific event level processors to the correct TTree writer
        if (channelCfg.eventStrategy == "LOLX") {
          std::shared_ptr<LoLXEvent> lolxEvent =
              std::dynamic_pointer_cast<LoLXEvent>(event);
          if (lolxEvent) {
            ws = new LoLXTTreeWriter(channelCfg.vars, dataSource, lolxEvent);
          } else {
            throw std::runtime_error(
                "Failed to cast Event pointer to LoLXEvent pointer.");
          }
        } else {
          ws = new TTreeWriter(channelCfg.vars, dataSource);
        }
      } else {
        ws = strategies.front();
        sharedWriter = true;
      }
    } else {
      throw std::runtime_error("Writing strategy not recognized");
    }

    if (ws != nullptr) {
      std::vector<std::string> missingParams = ws->checkParams(channelCfg.vars);
      if (!missingParams.empty()) {
        std::string missingParamsStr = "";
        for (auto param : missingParams) {
          missingParamsStr.append("\n" + param);
        }
        throw std::runtime_error(
            "The following variables necessary for pulse writting are unset:" +
            missingParamsStr);
      }
    }

    strategies.push_back(ws);
    vars.push_back(channelCfg.vars);
  }
}

/**
 * Processes Event objects to find pulses.
 *
 * Iterates over the Event's channels and the data read in from the
 * ChannelConfigs simultaneously to pass the right context along with each
 * Waveform.
 *
 * Returns the total number of pulses written.
 */
int PulseWriteProcessor::processEvent() {
  int pulseCounter = 0;
  auto channels = mEvent->getChannels();

  for (int i = 0; i < channels->size(); i++) {
    Channel channel = channels->at(i);
    Waveform *waveform = channel.getWaveform();
    if (strategies[i] == nullptr)
      continue;
    strategies[i]->push(mEvent->getID(), channel, i, *waveform, vars.at(i),
                        mEvent->getTriggerTime());
    strategies[i]->pushEventVariables();

    pulseCounter += channels->at(i).getPulses()->size();
  }

  return pulseCounter;
}

void PulseWriteProcessor::write() {
  for (int i = 0; i < strategies.size(); i++) {
    if (strategies[i] != nullptr)
      strategies[i]->write();
  }
}

PulseWriteProcessor::~PulseWriteProcessor() {
  if (sharedWriter) {
    delete strategies[0];
  } else {
    for (IPulseWritingStrategy *strategy : this->strategies) {
      delete strategy;
    }
  }
}
