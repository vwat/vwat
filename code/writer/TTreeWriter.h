/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTREE_WRITER_H
#define TTREE_WRITER_H

#include "../reader/DataFile.h"
#include "PulseWritingStrategy.h"
#include "TFile.h"
#include "TROOT.h"
#include "TTree.h"
#include "TTreeBranch.h"
#include <string>

/**
 * IPulseWritingStrategy implementation of a TTree.
 * A single tree is created with a branch for each Channel, a branch for eventID
 * and triggerTime. Each branch for a Channel is a TTreeBranch.
 *
 * TTree Structure:
 * \startuml
 * !include ../docs/ttree.puml
 * \enduml
 */
class TTreeWriter : public IPulseWritingStrategy {
public:
  TTreeWriter(Json::Value cfg, DataSource dataSource);
  void write();
  bool push(int indexEvent, Channel &ch, int chID, Waveform &wf,
            Json::Value cfg, double eventTriggerTime);
  virtual void pushEventVariables(){};
  std::vector<std::string> checkParams(Json::Value params);
  ~TTreeWriter();

protected:
  TDirectory *mProcDir;
  TFile *mOutputFile;
  TTree *mTree;
  std::vector<TTreeBranch *> branches;
  bool written;
  Int_t eventID;        /*!< ID of the Event. */
  Double_t triggerTime; /*!< Trigger time for each Event. */
};

#endif // TTREE_WRITER_H
