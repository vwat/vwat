/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PFSWidth.h"
#include "TROOT.h"
#include <cmath>
#include <string>

/**
 * Identifies Pulses in the Waveform and returns them in a vector.
 *
 * Pulses are identified by significant deviation in the waveform off the the
 * noise floor. The the max (off of the basline) value within a certain period
 * of time in found as sets as the peak. The change is also calculated to insure
 * that it is a physical pulse. The pulses are added to a vector on the given
 * channel
 *
 * Note: Passed all tests and acts the same as the original 'Pulse Finding
 * Algorithm'
 */
std::vector<Pulse> PFSWidth::findPulses(Waveform &wf, Json::Value cfg) {
  std::vector<Pulse> pulses;

  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double fallTimeTauCoefficient = cfg["FallTimeTauCoefficient"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  double noise = cfg["Noise"].asDouble();
  int polarity = cfg["Polarity"].asInt();
  double prominence = cfg["Prominence"].asDouble();
  int waveformStart = cfg["WaveformStart"].asInt();
  int waveformEnd = cfg["WaveformEnd"].asDouble();
  int minPulseWidth = cfg["MinPulseWidth"].asInt();
  int pulseStartCalcOffset = cfg["PulseStartCalcOffset"].asInt();
  int pulseStartNoiseThresholdCoefficient =
      cfg["PulseStartNoiseThresholdCoefficient"].asInt();
  double pulseEndNoiseThresholdCoefficient =
      cfg["PulseEndNoiseThresholdCoefficient"].asDouble();

  double baselineMu = wf.getWaveformBaseline().mu;
  if (!baselineMu) {
    // skip noisy waveform
    return pulses;
  }

  int numBins = wf.GetNbinsX() - waveformEnd;
  double binWidth = wf.GetBinWidth(1);

  int binRiseTime = round(riseTimeSigma * riseTimeSigmaCoefficient / binWidth);
  int binFallTime =
      round((fallTimeTau * fallTimeTauCoefficient + fallTime2Tau) / binWidth);

  for (int bin = binRiseTime + 1 + waveformStart; bin <= numBins; bin++) {
    double binAmp = wf.GetBinContent(bin);

    if (polarity * (binAmp - baselineMu) <= (noise * prominence)) {
      // didn't meet noise threshold
      continue;
    }
    double pulseAbsAmp = binAmp;

    bin++;
    while (bin <= wf.GetNbinsX() &&
           pulseAbsAmp <= polarity * wf.GetBinContent(bin)) {
      pulseAbsAmp = polarity * wf.GetBinContent(bin);
      bin++;
    }

    int lastBin = std::min(bin + binFallTime, wf.GetNbinsX());
    // find the pulse's starting point
    int pulseStartBin;
    for (pulseStartBin = bin - binRiseTime - pulseStartCalcOffset;
         pulseStartBin < lastBin; pulseStartBin++) {
      double noiseThreshold = pulseStartNoiseThresholdCoefficient * noise;
      double absAmpDelta =
          polarity * (wf.GetBinContent(pulseStartBin) - baselineMu);

      if (absAmpDelta >= noiseThreshold) {
        break;
      }
    }

    // find the pulse's ending point
    // TODO: maybe set something like "pulseUpperBound" to avoid doing this
    // calculation a bunch of times
    int pulseEndBin;
    for (pulseEndBin = bin; pulseEndBin < bin + binFallTime; pulseEndBin++) {
      double noiseThreshold = pulseEndNoiseThresholdCoefficient * noise;
      double absAmpDelta =
          polarity * (wf.GetBinContent(pulseEndBin) - baselineMu);

      if (absAmpDelta <= noiseThreshold) {
        break;
      }
    }

    int pulseWidth = pulseEndBin - pulseStartBin;

    bool pulseWidthCut = pulseWidth > minPulseWidth;
    // std::cout << std::endl << "----" << pulseWidth << std::endl;
    if (pulseWidthCut) {
      bin--;

      Pulse pulse{wf.GetBinCenter(bin),                  // time
                  pulseAbsAmp,                           // absAmp
                  polarity * (pulseAbsAmp - baselineMu), // amp
                  baselineMu,                            // baseline
                  -1,                                    // charge
                  pulseWidth,                            // width
                  -1,
                  FitPulse()};
      pulses.push_back(pulse);
      // move bin index to the end of the pulse
      bin += binFallTime;
    } else {
      bin++;
    }
  }

  return pulses;
}

/**
 * Checks the runtime parameters and returns a boolean indicating whether or not
 * they contain every value that the algorithm depends on. It's a bit ugly, but
 * pre-checking those parameters lets the program fail fast.
 */
std::vector<std::string> PFSWidth::checkParams(Json::Value params) {

  std::vector<std::string> requiredParams = {
      "Noise",
      "RiseTimeSigma",
      "FallTimeTau",
      "FallTime2Tau",
      "Polarity",
      "RiseTimeSigmaCoefficient",
      "FallTimeTauCoefficient",
      "PulseStartCalcOffset",
      "PulseStartNoiseThresholdCoefficient",
      "PulseEndNoiseThresholdCoefficient",
      "MinPulseWidth",
      "WaveformStart",
      "WaveformEnd",
      "Prominence"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
