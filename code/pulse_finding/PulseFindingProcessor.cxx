/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PulseFindingProcessor.h"
#include "LargestValue.h"
#include "PFS.h"
#include "PFSAvergeBinThreshold.h"
#include "PFSPolarity.h"
#include "PFSProminence.h"
#include "PFSWidth.h"

/**
 * PulseFindingProcessor constructor.
 *
 * Takes ChannelConfigs (one per channel) and maps their contents to pair of
 * vectors. When adding new a #Pulse Finding strategy class, register it in
 * this method along with a name that can be used in the JSON config file to
 * select it.
 */
PulseFindingProcessor::PulseFindingProcessor(
    std::vector<ChannelConfig> channelConfigs, std::shared_ptr<Event> event)
    : mEvent(event) {
  for (auto channelCfg : channelConfigs) {
    std::unique_ptr<IPulseFindingStrategy> pfs;
    auto pfsName = channelCfg.pulseFindingStrategy;

    if (pfsName == "PFS") {
      pfs = std::make_unique<PFS>();
    } else if (pfsName == "PFSWidth") {
      pfs = std::make_unique<PFSWidth>();
    } else if (pfsName == "PFSPolarity") {
      pfs = std::make_unique<PFSPolarity>();
    } else if (pfsName == "PFSProminence") {
      pfs = std::make_unique<PFSProminence>();
    } else if (pfsName == "PFSProminenceAverage") {
      pfs = std::make_unique<PFSAvergeBinThreshold>();
    } else if (pfsName == "LargestValue") {
      pfs = std::make_unique<LargestValue>();
    } else {
      throw std::runtime_error("Pulse finding strategy not recognized");
    }

    std::vector<std::string> missingParams = pfs->checkParams(channelCfg.vars);
    if (!missingParams.empty()) {
      std::string missingParamsStr = "";
      for (auto param : missingParams) {
        missingParamsStr.append("\n" + param);
      }
      throw std::runtime_error(
          "The following variables necessary for pulse finding are unset:" +
          missingParamsStr);
    }

    strategies.push_back(std::move(pfs));
    vars.push_back(channelCfg.vars);
  }
}

/**
 * Processes Event objects to find pulses.
 *
 * Iterates over the Event's channels and the data read in from the
 * ChannelConfigs simultaneously to pass the right context along with each
 * Waveform.
 *
 * Returns the total number of pulses found in all channels of the Event.
 */
int PulseFindingProcessor::processEvent() {
  int pulseCounter = 0;
  auto channels = mEvent->getChannels();

  for (int i = 0; i < channels->size(); i++) {
    auto channel = (*channels)[i];
    auto pulses =
        strategies[i]->findPulses(*(channels->at(i).getWaveform()), vars[i]);

    channels->at(i).setPulses(pulses);
    pulseCounter += pulses.size();
  }

  return pulseCounter;
}
