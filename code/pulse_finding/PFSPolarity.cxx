/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PFSPolarity.h"
#include "TROOT.h"
#include <cmath>
#include <string>

/**
 * Identifies Pulses in the Waveform and returns them in a vector.
 *
 * Pulses are identified by significant deviation in the waveform off the the
 * noise floor. The the max (off of the basline) value within a certain period
 * of time in found as sets as the peak. The change is also calculated to insure
 * that it is a physical pulse. The pulses are added to a vector on the given
 * channel
 *
 * Note: Passed all tests and acts the same as the original 'Pulse Finding
 * Algorithm'
 */
std::vector<Pulse> PFSPolarity::findPulses(Waveform &wf, Json::Value cfg) {
  std::vector<Pulse> pulses;

  int numBins = cfg["NumBins"].asInt();
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double fallTimeTauCoefficient = cfg["FallTimeTauCoefficient"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  double noise = cfg["Noise"].asDouble();
  int polarity = cfg["Polarity"].asInt();
  int pulseBaselineCalcBeginOffset =
      cfg["PulseBaselineCalcBeginOffset"].asInt();
  int pulseBaselineCalcEndOffset = cfg["PulseBaselineCalcEndOffset"].asInt();
  int pulseChargeCalcOffset = cfg["PulseChargeCalcOffset"].asInt();
  int pulseStartCalcOffset = cfg["PulseStartCalcOffset"].asInt();
  int pulseStartNoiseThresholdCoefficient =
      cfg["PulseStartNoiseThresholdCoefficient"].asInt();
  double pulseEndNoiseThresholdCoefficient =
      cfg["PulseEndNoiseThresholdCoefficient"].asDouble();
  int pulseChargeCutCoefficient = cfg["PulseChargeCutCoefficient"].asInt();
  int pulseBaselineDifferenceCutCoefficient =
      cfg["PulseBaselineDifferenceCutCoefficient"].asInt();
  double pulseOppositePolarityAmpCutCoefficient =
      cfg["PulseOppositePolarityAmpCutCoefficient"].asDouble();
  double prominenceThreshold = cfg["Prominence"].asDouble();
  double riseTimeThreshold = cfg["RiseTimeThreshold"].asDouble();
  int minPulseWidth = cfg["MinPulseWidth"].asInt();

  double baselineMu = wf.getWaveformBaseline().mu;
  if (!baselineMu) {
    // skip noisy waveform
    return pulses;
  }

  numBins = numBins ? numBins : wf.GetNbinsX();
  double binWidth = wf.GetBinWidth(1);

  int binRiseTime = round(riseTimeSigma * riseTimeSigmaCoefficient / binWidth);
  int binFallTime =
      round((fallTimeTau * fallTimeTauCoefficient + fallTime2Tau) / binWidth);

  for (int bin = pulseBaselineCalcBeginOffset + binRiseTime + 1; bin <= numBins;
       bin++) {
    double binAmp = wf.GetBinContent(bin);

    bool cond1 =
        polarity * (binAmp - baselineMu) > (noise * prominenceThreshold);
    bool cond2 = polarity * (binAmp - wf.GetBinContent(bin - binRiseTime)) >
                 (noise * riseTimeThreshold);

    if (!(cond1 && cond2)) {
      // didn't meet noise threshold
      continue;
    }
    double pulseAbsAmp = polarity * binAmp;

    bin++;
    while (bin <= wf.GetNbinsX() &&
           pulseAbsAmp <= polarity * wf.GetBinContent(bin)) {
      pulseAbsAmp = polarity * wf.GetBinContent(bin);
      bin++;
    }

    pulseAbsAmp *= polarity;

    // calculate baseline before pulse
    double pulseBaseline{0.0};
    double prePulseMin{0.0};

    int baseline_start_bin =
        std::max(bin - binRiseTime - pulseBaselineCalcBeginOffset, 0);
    int baseline_stop_bin = bin - binRiseTime - pulseBaselineCalcEndOffset;
    for (int tmpBin = baseline_start_bin; tmpBin < baseline_stop_bin;
         tmpBin++) {
      double tmpBinAmp = wf.GetBinContent(tmpBin);
      double absTmpBinAmp = polarity * tmpBinAmp;

      pulseBaseline += tmpBinAmp;
      if (absTmpBinAmp < prePulseMin) {
        prePulseMin = absTmpBinAmp;
      }
    }

    if (baseline_start_bin == 0)
      pulseBaseline /= (baseline_stop_bin - 1);
    else
      pulseBaseline /= (baseline_stop_bin - baseline_start_bin);

    bool chargeCut =
        false; // checking to see if the waveform swings across the baseline
    double relBinAmp = polarity * (binAmp - pulseBaseline);

    int lastBin = std::min(bin + binFallTime, wf.GetNbinsX());
    // calculate the charge
    double charge = 0.0;
    for (int tmpBin = (bin - binRiseTime - pulseChargeCalcOffset);
         tmpBin < lastBin; tmpBin++) {
      charge += wf.GetBinContent(tmpBin);
      double absAmpDelta =
          polarity * (wf.GetBinContent(tmpBin) - pulseBaseline);
      if (absAmpDelta < -pulseOppositePolarityAmpCutCoefficient * relBinAmp) {
        chargeCut = true;
        break;
      }
    }

    if (chargeCut)
      continue;

    charge -=
        baselineMu * (binRiseTime + lastBin - bin + pulseChargeCalcOffset);

    charge *= polarity;
    charge *= wf.GetBinWidth(1);

    // find the pulse's starting point
    int pulseStartBin;
    for (pulseStartBin = bin - binRiseTime - pulseStartCalcOffset;
         pulseStartBin < lastBin; pulseStartBin++) {
      double noiseThreshold = pulseStartNoiseThresholdCoefficient * noise;
      double absAmpDelta =
          polarity * (wf.GetBinContent(pulseStartBin) - pulseBaseline);

      if (absAmpDelta >= noiseThreshold) {
        break;
      }
    }

    // find the pulse's ending point
    // TODO: maybe set something like "pulseUpperBound" to avoid doing this
    // calculation a bunch of times
    int pulseEndBin;
    for (pulseEndBin = bin; pulseEndBin < bin + binFallTime; pulseEndBin++) {
      double noiseThreshold = pulseEndNoiseThresholdCoefficient * noise;
      double absAmpDelta =
          polarity * (wf.GetBinContent(pulseEndBin) - pulseBaseline);

      if (absAmpDelta <= noiseThreshold) {
        break;
      }
    }

    int pulseWidth = pulseEndBin - pulseStartBin;

    bool pulseInBounds = bin < wf.GetNbinsX();
    bool pulseChargeCut = charge > pulseChargeCutCoefficient * noise *
                                       std::sqrt(binRiseTime + binFallTime);
    bool pulseBaselineDistanceCut =
        (-1 * prePulseMin - baselineMu) <
        pulseBaselineDifferenceCutCoefficient * noise;
    bool pulseWidthCut = pulseWidth > minPulseWidth;
    // std::cout << std::endl << "----" << pulseWidth << std::endl;
    if (pulseInBounds && pulseChargeCut && pulseBaselineDistanceCut &&
        pulseWidthCut) {
      bin--;

      Pulse pulse{wf.GetBinCenter(bin),                     // time
                  pulseAbsAmp,                              // absAmp
                  polarity * (pulseAbsAmp - pulseBaseline), // amp
                  pulseBaseline,                            // baseline
                  charge,                                   // charge
                  pulseWidth,                               // width
                  -1,
                  FitPulse()};
      pulses.push_back(pulse);
      // move bin index to the end of the pulse
      bin += binFallTime;
    } else {
      bin++;
    }
  }

  return pulses;
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string> PFSPolarity::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {
      "Noise",
      "RiseTimeSigma",
      "FallTimeTau",
      "FallTime2Tau",
      "Polarity",
      "RiseTimeSigmaCoefficient",
      "FallTimeTauCoefficient",
      "PulseBaselineCalcBeginOffset",
      "PulseBaselineCalcEndOffset",
      "PulseChargeCalcOffset",
      "PulseStartCalcOffset",
      "PulseStartNoiseThresholdCoefficient",
      "PulseEndNoiseThresholdCoefficient",
      "PulseChargeCutCoefficient",
      "PulseOppositePolarityAmpCutCoefficient",
      "MinPulseWidth",
      "PulseBaselineDifferenceCutCoefficient",
      "RiseTimeThreshold",
      "Prominence"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
