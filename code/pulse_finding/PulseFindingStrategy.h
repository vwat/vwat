/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PULSEFINDINGSTRATEGY_H
#define __PULSEFINDINGSTRATEGY_H

#include "../data_structures/Pulse.h"
#include "../data_structures/Waveform.h"
#include <jsoncpp/json/json.h>
#include <vector>

/**
 * An interface for #Pulse Finding strategy implementations. If you want to
 * develop a new #Pulse Finding algorithm, have it inherit from this class and
 * override the two virtual methods (refer to PFS for an example). Designed to
 * be called from PulseFindingProcessor.
 */
class IPulseFindingStrategy {
public: // TODO: can the const be added back to the waveform
  virtual std::vector<Pulse> findPulses(Waveform &wf, Json::Value cfg) = 0;
  virtual std::vector<std::string> checkParams(Json::Value params) = 0;
  virtual ~IPulseFindingStrategy() {}
};

#endif // __PULSEFINDINGSTRATEGY_H
