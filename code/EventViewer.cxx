/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EventViewer.h"
#include "TApplication.h"
#include "TGClient.h"
#include "TLine.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVirtualFFT.h"
#include "data_structures/FitPulse.h"

#define WAVEFORM_BUTTON 0
#define POWER_SECTRUM_BUTTON 1
#define BASELINE_BUTTON 2

/**
 * This is where yu put the stopping condition
 */
bool condition(std::vector<Pulse> *pulses) { return true; }

void EventViewerFrame::setCurrentView(int new_chID) {
  if (new_chID < 0) {
    this->numChannels = srm->dataSource.channels;
    fEcanvas->GetCanvas()->Divide(h_divide, ceil(this->numChannels / h_divide));
    this->chID = -1;
  } else {
    fEcanvas->GetCanvas()->Divide();
    this->numChannels = 1;
    this->chID = new_chID;
  }
}

EventViewerFrame::~EventViewerFrame() {
  fMain->Cleanup();
  delete fMain;
}

void EventViewerFrame::HandleViewMenu(Int_t i) {
  TCanvas *fCanvas = fEcanvas->GetCanvas();
  fCanvas->cd();
  fCanvas->Clear();

  this->setCurrentView(i - 1);

  draw();

  fCanvas->Update();
}

void EventViewerFrame::HandleModeMenu(Int_t i) {
  TCanvas *fCanvas = fEcanvas->GetCanvas();

  this->drawMode = i;

  // this->setCurrentView(this->chID);
  draw();
  fCanvas->Update();
}

void EventViewerFrame::HandleSplineMenu(Int_t i) {
  if (chID >= 0) {
    TCanvas *fCanvas = fEcanvas->GetCanvas();
    fCanvas->cd(chID + 1);

    switch (i) {
    case 0:
      if (spline == nullptr && ch_pulses[chID]->size() > 0) {
        FitPulse *fit = &ch_pulses[chID]->front().fit;
        spline = new SplineWidget(8, fit->lowLimit, fit->highLimit,
                                  fit->baseline, fit->time);
        fCanvas->Update();
      } else {
        // delete spline;
      }
      break;

    case 1:
      if (spline != nullptr)
        spline->print();
      break;

    default:
      break;
    }
  }
}

void PolyMarker::ExecuteEvent(Int_t event, Int_t px, Int_t py) {
  TMarker::ExecuteEvent(event, px, py);
  spline->draw();
}

TH1D *EventViewerFrame::createBaselineHist(int ch, Waveform *wf) {
  TString name = "baseline_hist_" + TString(std::to_string(ch));
  TH1D *hist = (TH1D *)gROOT->FindObject(name);
  if (hist != nullptr)
    hist->Delete();
  Baseline b = wf->getWaveformBaseline();
  hist = new TH1D(name, TString(ch), 200, b.mu - b.RMS * 8, b.mu + b.RMS * 4);
  this->baseline_hists[ch] = hist;

  for (int i = 0; i < wf->GetNbinsX(); i++) {
    hist->Fill(wf->GetBinContent(i));
  }

  return hist;
}

TH1 *EventViewerFrame::createPowerSpectraHist(int ch, Waveform *wf) {
  TString name = "power_spectra_hist_" + TString(std::to_string(ch));
  TH1 *hist = (TH1 *)gROOT->FindObject(name);
  if (hist != nullptr)
    hist->Delete();
  hist = 0;

  TVirtualFFT::SetTransform(0);
  hist = wf->FFT(hist, "MAG");
  hist->SetName(name);
  this->power_spectra[ch] = hist;

  return hist;
}

/*
 * Constructor initalizing root graphics and the RunManager
 * Reads and processes the first specified event using the provided config file
 */
EventViewerFrame::EventViewerFrame(const TGWindow *p, UInt_t w, UInt_t h,
                                   int chID, int eventID,
                                   std::string config_path, int h_divide,
                                   int numSplineKnots)
    : TGMainFrame(p, w, h), numSplineKnots(numSplineKnots) {
  this->h_divide = h_divide;

  drawMode = 0;

  // Connect("CloseWindow()", "MainFrame", this, "DoExit()");// DontCallClose();

  std::unique_ptr<RunManager> rm =
      std::unique_ptr<RunManager>(new StepRunManager(eventID));
  RunManager::fromConfigFile(rm, config_path);
  std::shared_ptr<RunManager> tmp = std::move(rm);
  srm = std::static_pointer_cast<StepRunManager>(tmp);

  fits = std::vector<TF1 *>(srm->dataSource.channels);
  baseline_hists = std::vector<TH1D *>(srm->dataSource.channels);
  ch_pulses = std::vector<Pulses *>(srm->dataSource.channels);
  power_spectra = std::vector<TH1 *>(srm->dataSource.channels);
  spline = nullptr;

  if (chID > srm->dataSource.channels) {
    std::cout << "Error! ChID of " << chID << " out of range" << std::endl;
    return;
  }

  this->chID = chID;
  fMain = new TGMainFrame(p, w, h);

  fEcanvas = new TRootEmbeddedCanvas("Ecanvas", fMain, w, h);
  setCurrentView(this->chID);

  TGHorizontalFrame *hframe = new TGHorizontalFrame(fMain, w, 40);

  // a popup menu
  auto fMenuView = new TGPopupMenu(gClient->GetRoot());
  auto fMenuMode = new TGPopupMenu(gClient->GetRoot());
  auto fMenuSpline = new TGPopupMenu(gClient->GetRoot());

  fMenuMode->AddEntry("&Waveform", WAVEFORM_BUTTON);
  fMenuMode->AddEntry("&Baseline", BASELINE_BUTTON);
  fMenuMode->AddEntry("&Power Spectrum", POWER_SECTRUM_BUTTON);

  fMenuView->AddEntry("&All", 0);
  fMenuView->AddSeparator();

  fMenuSpline->AddEntry("Spline&3", 0);
  fMenuSpline->AddEntry("&Print", 1);

  for (int i = 0; i < srm->dataSource.channels; i++) {
    TString element_name = "Ch&" + std::to_string(i);
    fMenuView->AddEntry(element_name, i + 1);
  }

  auto fMenuBarItemLayout =
      new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
  auto fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);
  // menu bar
  auto fMenuBar = new TGMenuBar(fMain, 1, 1, kHorizontalFrame);
  // adding popup menus
  fMenuBar->AddPopup("&View", fMenuView, fMenuBarItemLayout);
  fMenuBar->AddPopup("&Spline", fMenuSpline, fMenuBarItemLayout);
  fMenuBar->AddPopup("&Mode", fMenuMode, fMenuBarItemLayout);
  fMenuView->Connect("Activated(Int_t)", "EventViewerFrame", this,
                     "HandleViewMenu(int)");
  fMenuSpline->Connect("Activated(Int_t)", "EventViewerFrame", this,
                       "HandleSplineMenu(int)");
  fMenuMode->Connect("Activated(Int_t)", "EventViewerFrame", this,
                     "HandleModeMenu(int)");

  // Don't forget this line !!!!
  fMain->AddFrame(fMenuBar, new TGLayoutHints(kLHintsTop | kLHintsExpandX));
  fMain->AddFrame(fEcanvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,
                                              10, 10, 10, 1));

  draw_button = new TGTextButton(hframe, "&Next");
  draw_button->Connect("Clicked()", "EventViewerFrame", this, "DoDraw()");
  hframe->AddFrame(draw_button, new TGLayoutHints(kLHintsCenterX, 5, 5, 3, 4));
  fMain->AddFrame(hframe, new TGLayoutHints(kLHintsCenterX, 2, 2, 2, 2));
  fMain->SetWindowName("Simple Example");
  fMain->MapSubwindows();
  fMain->Resize(fMain->GetDefaultSize());
  fMain->MapWindow();

  srm->start();
  process();
  draw();
}

void EventViewerFrame::DoDraw() {
  TCanvas *fCanvas = fEcanvas->GetCanvas();
  fCanvas->cd();
  do {
    srm->next();
  } while (!process());
  draw();
  fCanvas->Update();
}

bool EventViewerFrame::process() {
  bool found_intrest = false;

  srm->pulseFinding();
  srm->pulseAnalysis();

  for (int ch = 0; ch < this->numChannels; ch++) {
    ch_pulses[ch] = srm->getPulse(ch);

    createBaselineHist(ch, srm->getWaveform(ch));
    createPowerSpectraHist(ch, srm->getWaveform(ch));

    if (ch_pulses[ch]->size() <= 0) {
      fits[ch] = nullptr;
      srm->getWaveform(ch)->SetLineColor(4);
      continue;
    }

    std::cout << "Ch: " << ch << std::endl;
    fits[ch] = srm->getFitFunction(ch_pulses[ch]->front(), ch);
    srm->setFitFunction(ch_pulses[ch], srm->getWaveform(ch), fits[ch],
                        ch); //"FExpRise"
    fits[ch]->SetLineStyle(1);

    if (condition(ch_pulses[ch])) {
      srm->getWaveform(ch)->SetLineColor(4);
      if (!found_intrest)
        found_intrest = true;
    } else {
      srm->getWaveform(ch)->SetLineColor(3);
    }
  }
  return found_intrest;
}

/*
 * Move to the next event, processes the event using the specified config file
 * The initally found pulses are plotted as triangle onto of the main waveform
 * The resulting fit is then overlayed.
 */
void EventViewerFrame::draw() {

  TCanvas *fCanvas = fEcanvas->GetCanvas();
  draw_button->SetText(
      std::string("Next (cur: " + std::to_string(srm->getEventID()) + ")")
          .c_str());

  for (int ch = std::max(this->chID, 0);
       ch < std::max(this->chID, 0) + this->numChannels; ch++) {
    fCanvas->cd(ch + 1);
    std::string title = std::to_string(ch);

    srm->getWaveform(ch)->SetTitle(title.c_str());
    switch (this->drawMode) {
    case WAVEFORM_BUTTON:
      srm->getWaveform(ch)->Draw();

      if (ch_pulses[ch]->size() <= 0)
        continue;
      std::cout << "Ch: " << ch << std::endl; // TODO: should this be in next?
      std::cout << "Pulses found...!" << std::endl;
      std::cout << "Trigger time = " << srm->getEventTriggerTime() << std::endl;

      RootHelper::drawPulses(ch_pulses[ch]);

      fCanvas->cd(ch + 1);

      fits[ch]->Draw("same");

      // TODO: drawing the lines should be based on a setting
      for (auto p = ch_pulses[ch]->begin(); p != ch_pulses[ch]->end(); ++p) {
        TH1D *filter_hist = srm->getFilter(ch);
        if (filter_hist != nullptr) {
          int nBins = filter_hist->GetXaxis()->GetNbins();
          for (int i = 0; i < nBins; i++) {
            filter_hist->SetBinContent(
                i, p->baseline - 40000 * filter_hist->GetBinContent(i) / 2);
          }
          filter_hist->GetXaxis()->Set(nBins, p->time, p->time + nBins * 2);
          filter_hist->Draw("same");
        }

        int min = srm->getWaveform(ch)->GetMinimum();
        int max = srm->getWaveform(ch)->GetMaximum();
        TLine *rise_line =
            new TLine(p->fit.lowLimit, min, p->fit.lowLimit, max);
        TLine *fall_line =
            new TLine(p->fit.highLimit, min, p->fit.highLimit, max);
        std::cout << p->fit.lowLimit << " " << p->fit.highLimit << std::endl;
        rise_line->Draw();
        fall_line->Draw();
      }

      break;
    case BASELINE_BUTTON:
      this->baseline_hists[ch]->Draw();
      break;
    case POWER_SECTRUM_BUTTON:
      this->power_spectra[ch]->SetMaximum(
          this->power_spectra[ch]->GetBinContent(5));
      this->power_spectra[ch]->Draw();
      break;

    default:
      break;
    }
  }
}

void EventViewer(std::string config_path = "experiments/debug.json",
                 int eventID = 0, int chID = -1, int h_divide = 1,
                 int numSplineKnots = 8) {
  gStyle->SetOptStat(0);
  gStyle->SetTitleW(0.1);
  gStyle->SetTitleH(0.1);
  EventViewerFrame *mf =
      new EventViewerFrame(gClient->GetRoot(), 800, 800, chID, eventID,
                           config_path, h_divide, numSplineKnots);
}

int main(int argc, char **argv) {
  if (argc < 2)
    return EXIT_FAILURE;

  int eventID = 0;
  int chID = -1;
  int h_divide = 1;
  int numSplineKnots = 8;

  std::string config_path = std::string(argv[1]);

  if (argc > 2)
    eventID = std::stoi(argv[2]);
  if (argc > 3)
    chID = std::stoi(argv[3]);
  if (argc > 4)
    h_divide = std::stoi(argv[4]);
  if (argc > 5)
    numSplineKnots = std::stoi(argv[5]);

  TApplication theApp("App", &argc, argv);
  // theApp.Run();

  EventViewer(config_path, eventID, chID, h_divide, numSplineKnots);
  theApp.Run();
  return EXIT_SUCCESS;
}