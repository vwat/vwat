/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EVENTVIEWER_H_
#define __EVENTVIEWER_H_

#include "../code/StepRunManager.h"
#include "TButton.h"
#include "TCanvas.h"
#include "TGButton.h"
#include "TGFrame.h"
#include "TGMenu.h"
#include "TGWindow.h"
#include "TMarker.h"
#include "TRootEmbeddedCanvas.h"
#include "TSpline.h"
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

class SplineWidget;

static SplineWidget *spline;

class PolyMarker : public TMarker {
private:
  // SplineWidget *sw;
public:
  using TMarker::TMarker;
  void ExecuteEvent(Int_t event, Int_t px, Int_t py) override;
};

class SplineWidget {
private:
  TSpline3 *spline3;
  std::vector<PolyMarker *> points;
  double baseline;
  double time;

public:
  inline SplineWidget(int N, double min, double max, double baseline,
                      double time, int style = 22) {
    spline3 = nullptr;
    this->baseline = baseline;
    this->time = time;
    double dx = fabs(max - min) / N;
    for (int i = 0; i < N; i++) {
      points.push_back(new PolyMarker(i * dx, baseline, style));
      points[i]->Draw();
    }
    draw();
  };

  inline void draw() {
    if (spline3 != nullptr) {
      spline3->Delete();
    }
    double xx[points.size()];
    double yy[points.size()];
    for (int i = 0; i < points.size(); i++) {
      xx[i] = points[i]->GetX();
      yy[i] = points[i]->GetY();
    }
    spline3 = new TSpline3("TestSpline", xx, yy, points.size(), "e1", 0);
    spline3->Draw("lsame");
    return;
  };

  inline void print() {
    double max_abs = 0;
    for (int i = 0; i < points.size(); i++) {
      if (fabs(baseline - points[i]->GetY()) > max_abs)
        max_abs = fabs(baseline - points[i]->GetY());
    }
    for (int i = 0; i < points.size(); i++) {
      std::cout << points[i]->GetX() - time << " "
                << (baseline - points[i]->GetY()) / max_abs << std::endl;
    }
  }
};

/*
 * Main display holding the canvas and window
 */
class EventViewerFrame : public TGMainFrame {
  typedef std::vector<Pulse> Pulses;

private:
  TGMainFrame *fMain;
  TRootEmbeddedCanvas *fEcanvas;
  std::shared_ptr<StepRunManager> srm;
  int chID;
  int numChannels;
  int h_divide;
  std::vector<TF1 *> fits;
  std::vector<TH1D *> baseline_hists;
  std::vector<Pulses *> ch_pulses;
  std::vector<TH1 *> power_spectra;
  TGTextButton *draw_button;
  int drawMode;
  int numSplineKnots;
  // SplineWidget* spline;
public:
  EventViewerFrame(const TGWindow *p, UInt_t w, UInt_t h, int chID, int eventID,
                   std::string config_pat, int h_divide, int numSplineKnots);
  void draw();
  bool process();
  ~EventViewerFrame();
  void DoDraw();
  void HandleSplineMenu(Int_t i);
  void setCurrentView(int i);
  void HandleViewMenu(Int_t i);
  void HandleModeMenu(Int_t i);
  TH1D *createBaselineHist(int ch, Waveform *wf);
  TH1 *createPowerSpectraHist(int ch, Waveform *wf);

  ClassDef(EventViewerFrame, 0);
};

#endif // __EVENTVIEWER_H_