/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LOLXEVENTPROCESSOR_H_
#define __LOLXEVENTPROCESSOR_H_

#include "../data_structures/LoLXEvent.h"
#include "EventLevelProcessor.h"

/**
 * Process the data on an event by event, after pulse analysis
 * Used for lolx to analysis
 * Calculates the multiplicity (number of phontons)
 */
class LoLXEventProcessor : public IEventLevelProcessor {
public:
  LoLXEventProcessor(std::shared_ptr<LoLXEvent> event);
  int processEvent() override;

private:
  std::shared_ptr<LoLXEvent> mEvent;
};

#endif // __EVENTPROCESSOR_H_
