/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __RUNMANAGER_H_
#define __RUNMANAGER_H_

#include "data_structures/LoLXEvent.h"
#include "event_analysis/EventLevelProcessor.h"
#include "reader/Reader.h"
#include <fstream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

/**
 * The RunManager class acts as a mediator between all subsystems and provides
 * an entrypoint for the program. Its responsibilities are to:
 *
 * 1. Read and validate the JSON-formatted experiment configuration file
 * 2. Call into the Reader subsystem to get data from the specified data file
 * 3. Call into various %event processors
 * 4. Write the resulting pulses and visualizations to disk so they can be
 *    loaded and examined further using ROOT
 */
class RunManager {
public:
  /**
   * Static factory method that generates a RunManager object based on a JSON
   * config file and returns a smart pointer pointing to it. To be used in
   * main().
   *
   * We decoupled this method from the `fromJson()` method in order to separate
   * file I/O logic from the JSON parsing itself. This should allow some
   * flexibility if developers want to introduce new config formats, or source
   * JSON objects from sources other than locally-available files.
   */
  static void fromConfigFile(std::unique_ptr<RunManager> &rm,
                             std::string path) {
    Json::Value rootCfgJson;
    std::ifstream configFile{path, std::ifstream::binary};
    configFile >> rootCfgJson;

    if (rm == nullptr) {
      rm = std::unique_ptr<RunManager>(new RunManager());
    }

    fromJson(rm, rootCfgJson);
  }

  /**
   * Constructs a RunManager object and returns a smart pointer to it. To be
   * called from either `fromConfigFile()` or `main()`.
   *
   * We tried to make the JSON schema for the config file fairly permissive in
   * the sense that you can add new fields and access them in later parts in the
   * program. That said, major (i.e. top-level) schema changes should be
   * validated here.
   */
  static void fromJson(std::unique_ptr<RunManager> &rm,
                       Json::Value rootCfgJson) {
    const auto eventLimit = rootCfgJson.get("EventLimit", 0).asInt();
    const auto dataSourceCfgJson{rootCfgJson["DataSource"]};
    const auto channelCfgJson{rootCfgJson["ChannelConfigs"]};

    if (eventLimit < 1) {
      throw std::runtime_error("Config JSON must contain a positive top-level "
                               "'EventLimit' attribute");
    }

    if (dataSourceCfgJson.isNull()) {
      throw std::runtime_error(
          "Config JSON must contain top-level 'DataSource' object");
    }

    if (channelCfgJson.isNull()) {
      throw std::runtime_error(
          "Config JSON must contain top-level 'Channels' array object");
    }

    const auto filePath = dataSourceCfgJson.get("FilePath", "").asString();
    const auto outFilePath =
        dataSourceCfgJson.get("OutputFilePath", "").asString();
    const auto device = dataSourceCfgJson.get("Device", "").asString();
    const auto eventType = dataSourceCfgJson.get("EventType", "").asString();
    const auto channels = dataSourceCfgJson.get("Channels", 0).asInt();
    auto vars = dataSourceCfgJson["Variables"];

    if (filePath.empty() || device.empty() || channels == 0) {
      throw std::runtime_error(
          "DataSource config object must contain: 'FilePath' (string), "
          "'Device' (string), 'Channels' (int)");
    }

    const auto ep = channelCfgJson[0].isMember("EventProcessor")
                        ? channelCfgJson[0].get("EventProcessor", "").asString()
                        : "NA";

    std::vector<ChannelConfig> channelConfigs;

    if (channelCfgJson.size() == channels) {
      for (auto channelCfg : channelCfgJson) {
        const auto fls = channelCfg.isMember("FilterStrategy")
                             ? channelCfg.get("FilterStrategy", "").asString()
                             : "NA";
        const auto bes = channelCfg.get("BaselineStrategy", "").asString();
        const auto pfs = channelCfg.get("PulseFindingStrategy", "").asString();
        const auto pas = channelCfg.get("PulseAnalysisStrategy", "").asString();
        const auto ws = channelCfg.get("WritingStrategy", "").asString();
        auto vars = channelCfg["Variables"];

        if (bes.empty() || pfs.empty() || pas.empty() || ws.empty() ||
            vars.isNull()) {
          throw std::runtime_error(
              "Each channel config object must contain: 'FilterStrategy' "
              "(string),'BaselineStrategy' (string), 'PulseFindingStrategy' "
              "(string), 'PulseAnalysisStrategy' (string), 'WritingStrategy' "
              "(string), 'EventProcessor' (string), 'Variables' (object)");
        }

        channelConfigs.push_back({fls, bes, pfs, pas, ep, ws, vars});
      }
    } else if (channelCfgJson.size() == 1) {
      const auto fls =
          channelCfgJson[0].isMember("FilterStrategy")
              ? channelCfgJson[0].get("FilterStrategy", "").asString()
              : "NA";
      const auto bes = channelCfgJson[0].get("BaselineStrategy", "").asString();
      const auto pfs =
          channelCfgJson[0].get("PulseFindingStrategy", "").asString();
      const auto pas =
          channelCfgJson[0].get("PulseAnalysisStrategy", "").asString();
      const auto ws = channelCfgJson[0].get("WritingStrategy", "").asString();
      auto vars = channelCfgJson[0]["Variables"];

      if (bes.empty() || pfs.empty() || pas.empty() || ws.empty() ||
          vars.isNull()) {
        throw std::runtime_error(
            "Each channel config object must contain: 'FilterStrategy' "
            "(string),'BaselineStrategy' (string), 'PulseFindingStrategy' "
            "(string), 'PulseAnalysisStrategy' (string), 'WritingStrategy' "
            "(string), 'EventProcessor' (string), 'Variables' (object)");
      }

      for (int i = 0; i < channels; i++) {
        channelConfigs.push_back({fls, bes, pfs, pas, ep, ws, vars});
      }
    } else {
      throw std::runtime_error(
          "Invalid 'ChannelConfigs'.\nInclude a single 'ChannelConfig' to "
          "repeat for all Channels or include one 'ChannelConfig' for each "
          "individual Channel.");
    }

    rm->eventLimit = eventLimit;
    rm->dataSource = {device, filePath, outFilePath, channels, vars, eventType};
    rm->channelConfigs = channelConfigs;
  }

  RunManager(){};
  void processEvents();

  int eventLimit; /*!< Limits the number of events that get processed. */
  DataSource
      dataSource; /*!< Defines the datasource providing Events for analysis */
  std::vector<ChannelConfig>
      channelConfigs; /*!< Holds strategy configs and variables for each channel
                         that gets analyzed */

protected:
  std::unique_ptr<Reader> createReader(std::shared_ptr<Event> &event);
  std::unique_ptr<IEventLevelProcessor>
  createEventLevelProcessor(std::shared_ptr<Event> &event);

  RunManager(int eventLimit, DataSource dataSource,
             std::vector<ChannelConfig> channelConfigs)
      : eventLimit{eventLimit}, dataSource{dataSource}, channelConfigs{
                                                            channelConfigs} {}
};

#endif // __RUNMANAGER_H_
