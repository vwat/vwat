/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Exp2GausMatchedFilter.h"
#include "../WaveformStatistic.h"

/**
 * Exp2GausMatchedFilterStrategy construcor calling parent
 * (MatchedFilterStrategy) construcor.
 */
Exp2GausMatchedFilterStrategy::Exp2GausMatchedFilterStrategy(Json::Value cfg)
    : MatchedFilterStrategy::MatchedFilterStrategy(cfg) {
  this->createPulseShapeKernel(cfg);
}

/**
 * Creates and initalized a FuncExpGausMulti used to generate the match filter
 */
void Exp2GausMatchedFilterStrategy::createPulseShapeFunction(Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();

  this->pulseShape = new TF1("pulseShape", FuncExp2GausMulti, 0, INT32_MAX, 8);

  this->pulseShape->ReleaseParameter(0);
  this->pulseShape->FixParameter(0, 0);
  this->pulseShape->FixParameter(1, riseTimeSigma);
  this->pulseShape->FixParameter(2, fallTimeTau);
  this->pulseShape->FixParameter(3, time2Frac);
  this->pulseShape->FixParameter(4, fallTime2Tau);
  this->pulseShape->FixParameter(5, 1);
  this->pulseShape->FixParameter(6, 1.0);
  this->pulseShape->FixParameter(7, 1.0);
}
