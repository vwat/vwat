/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MeanFilter.h"

/**
 * Genrates the constat kernel used for the moving average
 */
MeanFilter::MeanFilter(Json::Value cfg) : ConvolutionFilterStrategy(cfg) {
  kernel = new double[kernelSize];

  for (int i = 0; i < kernelSize; i++)
    kernel[i] = 1.0 / kernelSize;
}
