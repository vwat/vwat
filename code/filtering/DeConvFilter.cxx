/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeConvFilter.h"
#include "TROOT.h"
#include "TString.h"

DeConvFilter::DeConvFilter() {}

/*
 * Unpacks the 'channel's Waveform and sets its waveform baseline value.
 * Return not implemented
 */
void DeConvFilter::filter(Channel &channel, Json::Value cfg) {

  int l_sum = cfg["KernelSize"].asInt();
  double beta = cfg["FallTimeTau"].asDouble();

  Waveform *h = channel.getWaveform();

  int l;
  int l_max;
  double s;
  double s_prev;
  int nBins = h->GetNbinsX();

  Waveform *h_d;

  TString p = h->GetName() + TString("_wmd");
  double base_line = h->getWaveformBaseline().mu;
  h_d = (Waveform *)h->Clone(p);

  //
  // Apply a moving window deconvolotion filter of width  l_sum  with decay
  // constant of  beta  on TH1D histogram  h  using flat baseline of  base_line
  // , Result is returned in cloned histogram . If  name  and/or  title  are set
  // use those for the resulting histogram. If no name is given use original
  // name with "_wmd" appended.
  //

  double t;

  l_max = h->GetXaxis()->GetNbins();

  s = 0;

  for (l = 0; l < l_max; ++l) {

    if (l_sum < l) {
      s -= h->GetBinContent(l - l_sum);
    } else {
      s -= base_line;
    }
    if (0 < l) {
      s += h->GetBinContent(l);
    } else {
      s += base_line;
    }

    if (l_sum <= l) {
      t = h->GetBinContent(1 + l - l_sum);
    } else {
      t = base_line;
    }
    t = s * beta + h->GetBinContent(1 + l) - t;
    h_d->SetBinContent(l + 1, t + base_line);
  }

  for (int i = 0; i < nBins; i++) {
    h->SetBinContent(i, h_d->GetBinContent(i));
  }

  h_d->Delete();
}
/**
 * Checks the runtime parameters and returns a boolean indicating whether or not
 * they contain every value that the algorithm depends on. It's a bit ugly, but
 * pre-checking those parameters lets the program fail fast.
 */
std::vector<std::string> DeConvFilter::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"KernelSize", "FallTimeTau"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
