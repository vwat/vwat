/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FilterProcessor.h"
#include "ConstantFractionFilter.h"
#include "DeConvFilter.h"
#include "Exp2GausMatchedFilter.h"
#include "ExpFilter.h"
#include "ExpMatchFilter.h"
#include "MeanFilter.h"
#include "TrapezoidalFilter.h"
#include "TriangleFilter.h"

FilterProcessor::FilterProcessor(std::vector<ChannelConfig> channelConfigs,
                                 std::shared_ptr<Event> event)
    : mEvent(event) {
  for (auto channelCfg : channelConfigs) {
    IFilterStrategy *fs;
    auto name = channelCfg.filterStrategy;

    if (name == "Triangle") {
      fs = new TriangleFilter(channelCfg.vars);
    } else if (name == "Trapezoidal") {
      fs = new TrapezoidalFilter();
    } else if (name == "DeConvFilter") {
      fs = new DeConvFilter();
    } else if (name == "ConstantFractionFilter") {
      fs = new ConstantFractionFilter();
    } else if (name == "Exp") {
      fs = new ExpFilterStrategy();
    } else if (name == "Mean") {
      fs = new MeanFilter(channelCfg.vars);
    } else if (name == "ExpMatch") {
      fs = new ExpMatchFilterStrategy(channelCfg.vars);
    } else if (name == "Exp2GausMatch") {
      fs = new Exp2GausMatchedFilterStrategy(channelCfg.vars);
    } else if (name == "NA") {
      fs = nullptr;
    } else {
      throw std::runtime_error("Filter strategy not recognized");
    }

    if (fs != nullptr) {
      std::vector<std::string> missingParams = fs->checkParams(channelCfg.vars);
      if (!missingParams.empty()) {
        std::string missingParamsStr = "";
        for (auto param : missingParams) {
          missingParamsStr.append("\n" + param);
        }
        throw std::runtime_error(
            "The following variables necessary for filtering are unset:" +
            missingParamsStr);
      }
    }

    strategies.push_back(fs);
    vars.push_back(channelCfg.vars);
  }
}

int FilterProcessor::processEvent() {
  auto channels = mEvent->getChannels();

  int pulseCounter;

  for (int i = 0; i < channels->size(); i++) {
    if (strategies[i] != nullptr) {
      strategies[i]->filter(channels->at(i), vars[i]);
      pulseCounter++;
    }
  }

  return pulseCounter;
}

TH1D *FilterProcessor::getFilter(int ch) {
  if (strategies[ch] == nullptr)
    return nullptr;

  int kernelSize = strategies[ch]->kernelSize;
  TH1D *filter = new TH1D("", ("filter" + std::to_string(ch)).c_str(),
                          kernelSize, 0, kernelSize * 2);
  for (int i = 0; i < kernelSize; i++) {
    filter->SetBinContent(i, strategies[ch]->kernel[i]);
  }

  return filter;
}