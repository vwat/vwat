/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstring>

#include "Proto0LaserFile.h"

/**
 * Proto0Laser constructor.
 *
 * The parameter `src` is used to set `numChannels`, `filePath` and get the
 * branch name. The `mEvent` member is set with the `event` parameter. A TFile
 * is opened using the filePath. A TTreeReader, TTreeReaderValue, and
 * TTreeReaderArray are created for use in #getNextEvent(). The waveforms are
 * initialized.
 *
 * The ROOT file is expected to contain a TTree named "T". The files contain
 * multiple branches for data from multiple sensors, the user must select a
 * single branch to analyze using the configuration file.
 */
Proto0LaserFile::Proto0LaserFile(DataSource src, std::shared_ptr<Event> event)
    : numChannels(src.channels), currentIndex(-1), eventId(0),
      mFile(src.filePath.c_str()), ttreeReader("T", &mFile),
      timestamp(ttreeReader, "timestamp"),
      branchData(ttreeReader, src.vars.get("Branch", "").asCString()),
      mEvent(event) {

  for (int ch = 0; ch < numChannels; ch++) {
    waveforms.push_back(new Waveform(2, 0., 2 * 2.));
  }
}

/**
 * Proto0Laser destructor.
 *
 * Deletes the waveforms. Closes the TFile.
 */
Proto0LaserFile::~Proto0LaserFile() {
  for (Waveform *wf : this->waveforms)
    wf->Delete();
  mFile.Close();
}

/**
 * Create and return the next Event.
 *
 * Reads an Event and gets the event data from the TTree.
 * A vector of channels is created and a Channel is added for each channel in
 * the experiment, `numChannels`. Each channel contains a single Waveform. The
 * member mEvent is updated using the `channels`, `eventId` and `triggerTime`.
 *
 * Returns a null pointer if the end of the file is reached.
 */
int Proto0LaserFile::getNextEvent() {
  bool readSuccessful = ttreeReader.Next();

  // Check that branches can be read.
  if (!readSuccessful && timestamp.GetSetupStatus() < 0) {
    throw std::runtime_error("\"timestamp\" branch was not found in TTree.");
  }
  if (!readSuccessful && branchData.GetSetupStatus() < 0) {
    throw std::runtime_error("\"DataSource.Variables\" must contain a valid "
                             "\"Branch\" entry. e.g. \"SIPM01\"");
  }

  if (!readSuccessful && eventId == ttreeReader.GetEntries()) {
    std::cout << "\nEnd of file reached." << std::endl;
    return -1;
  }

  std::vector<Channel> channels;

  for (int i = 0; i < numChannels; i++) {
    int nBins = branchData.GetSize();

    waveforms[i]->SetBins(nBins, 0., nBins * 2.);

    for (int iBin = 0; iBin < nBins; iBin++) {
      waveforms[i]->SetBinContent(iBin, branchData[iBin]);
    }
    Channel channel(waveforms[i], i);
    channels.push_back(channel);
  }

  // TODO: The timestamp might not be the trigger time. The trigger time may not
  // actually be in the file.
  double triggerTime = *(timestamp.Get());
  mEvent->addChannels(channels);
  mEvent->setID(eventId);
  mEvent->setTriggerTime(triggerTime);
  eventId++;

  return 0;
}

/**
 * Retrieve the Waveform at a given index.
 *
 * Calls #getNextEvent() and increments `currentIndex` up to `index`. Returns
 * waveform at this index from the file.
 *
 * Do not use this method if you are also using a Reader to read through the
 * file as the currentIndex will not be incremented properly.
 *
 * If using multi-channel data, this will get the waveform from the last
 * channel. In cases where this method will only be called once, it can be
 * replaced by creating a Reader with the specific DataFileFactory class and
 * calling Reader#next() the desires amount of times.
 */
Waveform *Proto0LaserFile::getWaveform(int index) {
  while (index > currentIndex) {
    getNextEvent();
    currentIndex++;
  }
  if (index < currentIndex)
    std::cerr << "Index too low. Cannot read backwards." << std::endl;
  return waveforms.back();
}

/**
 * Create and return an iterator (Reader) for Proto0Laser.
 *
 * This allows the RunManager to read through the events in the Midas file.
 */
std::unique_ptr<Reader> Proto0LaserFile::createIterator() {
  return std::make_unique<Reader>(this);
}
