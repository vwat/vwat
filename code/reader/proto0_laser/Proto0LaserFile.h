/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO0_LASER_FILE_H
#define PROTO0_LASER_FILE_H

#include "../DataFile.h"
#include "../Reader.h"
#include "TFile.h"
#include "TROOT.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "TTreeReaderValue.h"

class Waveform;

/**
 * Proto0Laser device implementation of a data file.
 */
class Proto0LaserFile : public DataFile {
public:
  Proto0LaserFile(DataSource src, std::shared_ptr<Event> event);
  int getNextEvent();
  Waveform *getWaveform(int index);
  std::unique_ptr<Reader> createIterator();

  ~Proto0LaserFile();

private:
  TFile mFile; /*!< ROOT file containing the experiment data. */
  std::vector<Waveform *> waveforms; /*!< Most recent waveform from the data. */
  int numChannels;  /*!< Number of channels for the experiment. */
  int currentIndex; /*!< Current index when using #getWaveform(int index). */
  int eventId;      /*!< Current ID for the Event. */
  TTreeReader ttreeReader;           /*!< Reader for the TTree in the file. */
  TTreeReaderValue<Int_t> timestamp; /*!< Timestamp from each Event. */
  TTreeReaderArray<UShort_t> branchData; /*!< Data from the selected branch. */
  std::shared_ptr<Event> mEvent;
};

#endif // PROTO0_LASER_FILE_H
