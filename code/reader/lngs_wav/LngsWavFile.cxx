/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LngsWavFile.h"

const int wordSizeBits = 16;

/**
 * WAV constructor.
 *
 * The parameter `src` is used to set `numChannels` and for the filePath. The
 * `mEvent` member is set with the `event` parameter. A WAV file is opened using
 * libsndfile and the filePath. If the data is 'LFoundry' data, the second
 * channel is the signal and they need to be swapped when saved.
 */
LngsWavFile::LngsWavFile(DataSource src, std::shared_ptr<Event> event)
    : numChannels(src.channels), currentIndex(-1),
      file(SndfileHandle(src.filePath)), offset(0), eventId(0), mEvent(event) {
  SNDFILE *sf = file.rawHandle();
  if (sf_error(sf)) {
    throw std::runtime_error(sf_strerror(sf));
  }

  // Check if channels need to be swapped.
  std::string fileName = src.filePath.substr(src.filePath.find_last_of("/") + 1,
                                             src.filePath.find(".wav"));
  swapChannels = (fileName.find("LF_TILE") == 0) ? true : false;

  for (int i = 0; i < numChannels; i++) {
    waveforms.push_back(new Waveform(2, 0., 2 * 2.));
  }
}

/**
 * WAV destructor.
 *
 * Deletes the waveforms.
 */
LngsWavFile::~LngsWavFile() {
  for (Waveform *wf : this->waveforms)
    wf->Delete();
}

/**
 * Create and return the next Event.
 *
 * Reads an LNGS WAV file and gets the header frame and data frame for each
 * Event. A vector of channels is created with `numChannel` entries. Each
 * channel contains a single Waveform. The member mEvent is updated using the
 * `channels`, `eventId` and `triggerTime`.
 *
 * For LNGS WAV files with 2 channels, the first channel is the signal and the
 * second channel is the trigger in the returned Event.
 *
 * Returns a null pointer if no data is read on the iteration.
 */
int LngsWavFile::getNextEvent() {
  RawEvent rawEvent;

  // Read header frame
  file.seek(offset, SEEK_SET);
  // Initally read the first 2 items to get the header size.
  std::vector<short int> headerFrame = readFileSection(file, 2);
  rawEvent.headerSize = (uint16_t)headerFrame[1];
  if (rawEvent.headerSize == 0) {
    std::cout << "\nEnd of file reached." << std::endl;
    return -1;
  }

  headerFrame.clear();
  file.seek(offset, SEEK_SET);
  // Read the remaining header data.
  headerFrame = readFileSection(file, rawEvent.headerSize);
  // Get low-order and high-order words for each item from the header frame.
  rawEvent.counter = toUint32(headerFrame[2], headerFrame[3]);
  rawEvent.timeTag = toUint32(headerFrame[4], headerFrame[5]);
  rawEvent.nSamples = toUint32(headerFrame[6], headerFrame[7]);
  rawEvent.nChannels = toUint32(headerFrame[10], headerFrame[11]);
  if (numChannels > rawEvent.nChannels) {
    throw std::runtime_error(
        "Requested number of channels is greater than available channels.");
  }

  // Read and decode data
  std::vector<short int> dataFrame = readFileSection(file, 2);
  file.seek(offset + rawEvent.headerSize, SEEK_SET);
  dataFrame = readFileSection(file, rawEvent.nChannels * rawEvent.nSamples);
  rawEvent.rawData = &dataFrame[0];

  offset += (toUint32(rawEvent.headerSize, 0) +
             rawEvent.nSamples * rawEvent.nChannels);

  std::vector<Channel> channels;
  int nBins = rawEvent.nSamples;
  for (int i = 0; i < numChannels; i++) {
    waveforms[i]->SetBins(nBins, 0., nBins * 2.);
    for (int iBin = 0; iBin < nBins; ++iBin) {
      waveforms[i]->SetBinContent(
          iBin, (uint16_t)rawEvent.rawData[iBin + i * rawEvent.nSamples]);
    }
    Channel channel(waveforms[i], i);
    if (swapChannels) {
      channels.insert(channels.begin(), channel);
    } else {
      channels.push_back(channel);
    }
  }

  mEvent->addChannels(channels);
  mEvent->setID(eventId);
  mEvent->setTriggerTime((double)rawEvent.timeTag);
  eventId++;

  return 0;
}

std::vector<short int> LngsWavFile::readFileSection(SndfileHandle &file,
                                                    int nItems) {
  std::vector<short int> fileData(nItems);
  file.read(&fileData[0], nItems);
  return fileData;
}

inline uint32_t LngsWavFile::toUint32(short int low, short int high) {
  return ((uint16_t)high << wordSizeBits) | (uint16_t)low;
}

/**
 * Retrieve the Waveform at a given index.
 *
 * Calls #getNextEvent() and increments `currentIndex` up to `index`. Returns
 * waveform at this index from the file.
 *
 * Do not use this method if you are also using a Reader to read through the
 * file as the currentIndex will not be incremented properly.
 */
Waveform *LngsWavFile::getWaveform(int index) {
  while (index > currentIndex) {
    getNextEvent();
    currentIndex++;
  }
  if (index < currentIndex)
    std::cerr << "Index too low. Cannot read backwards." << std::endl;
  return waveforms.back();
}

/**
 * Create and return an iterator (Reader) for Wav.
 *
 * This allows the RunManager to read through the events in the wav file.
 */
std::unique_ptr<Reader> LngsWavFile::createIterator() {
  return std::make_unique<Reader>(this);
}
