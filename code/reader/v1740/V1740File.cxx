/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstring>

#include "../../data_structures/Channel.h"
#include "../../data_structures/Waveform.h"
#include "TV1740Data.h"
#include "V1740File.h"

/**
 * V1740 constructor.
 *
 * The parameter `src` is used to set `numChannels` and `filePath`. The `mEvent`
 * member is set with the `event` parameter. A Midas file is opened using the
 * filePath, and the first event is read. The Y scale limits are set to the
 * values from #V1740YLimits.
 */
V1740File::V1740File(DataSource src, std::shared_ptr<LoLXEvent> event)
    : numChannels(src.channels), currentIndex(-1), midasFile(new TMidasFile()),
      midasEvent(new TMidasEvent()), dataContainer(new TDataContainer()),
      eventId(0), mEvent(event) {
  std::string filePath = src.filePath;

  if (!midasFile->Open(filePath.c_str())) {
    throw std::runtime_error("Cannot open input file: " + filePath);
  } else {
    std::cout << "Opening " << filePath << std::endl;
  }

  midasFile->Read(midasEvent);
  int midasEventId = midasEvent->GetEventId();
  if ((midasEventId & 0xFFFF) !=
      static_cast<int>(eventIdValue::beginRun)) { // begin run event
    throw std::runtime_error("First event Id does not match expectation.");
  }

  numBins = static_cast<int>(V1740YLimits::numBins);
  ampMax = static_cast<double>(V1740YLimits::ampMax);
  ampMin = static_cast<double>(V1740YLimits::ampMin);

  for (int ch = 0; ch < src.channels; ch++)
    waveforms.push_back(new Waveform(2, 0., 2 * 2.)); // init waveforms
}

/**
 * V1740 destructor.
 *
 * Deletes the waveform, midas event and data container. Closes the midas file.
 */
V1740File::~V1740File() {
  for (Waveform *wf : this->waveforms)
    wf->Delete();
  delete midasEvent;
  delete dataContainer;
  midasFile->Close();
  delete midasFile;
}

/**
 * Create and return the next Event.
 *
 * Reads a Midas event and gets the event data from the data container.
 * A vector of channels is created and a Channel is added for each channel in
 * the experiment, `numChannels`. Each channel contains a single Waveform. The
 * member mEvent is updated using the `channels`, `eventId` and `triggerTime`.
 *
 * Returns -1 if the end of the run is reached.
 */
int V1740File::getNextEvent() {
  TV1740Data *v1740 = nullptr;
  while (!v1740) {
    midasFile->Read(midasEvent);
    int midasEventId = midasEvent->GetEventId(); // read next event
    if ((midasEventId & 0xFFFF) ==
        static_cast<int>(eventIdValue::endRun)) { // end run event
      std::cout << "End of run reached" << std::endl;
      return -1;
    }
    midasEvent->SetBankList();
    // Set the midas event pointer in the physics event.
    dataContainer->SetMidasEventPointer(*midasEvent);

    v1740 = dataContainer->GetEventData<TV1740Data>("W400");
    if (!v1740)
      dataContainer->CleanupEvent();
  }

  if (!v1740) {
    throw std::runtime_error("No V1740 data.");
  }

  double triggerTime = v1740->GetTriggerTag() * 8;

  // std::vector<RawChannelMeasurement> measurements = v1740->GetMeasurements();
  std::vector<Channel> channels;

  // Create a waveform and add it to a Channel for each channel used in the
  // experiment.
  for (int i = 0; i < numChannels; i++) {
    // Check if this channel has any data in this event.
    int chhex = 1 << i;
    int chgrp = ((int)(i / 8));
    int chgrphex = 1 << chgrp;
    if (!(v1740->GetGroupMask() & chgrphex)) {
      continue;
    }

    int index = i;
    TV1740Channel channelData = v1740->GetChannelData(i);
    int nBins = channelData.GetNSamples();

    waveforms[i]->SetBins(nBins, 0., nBins * 2.);

    for (int iBin = 0; iBin < nBins; iBin++) {
      double adc = channelData.GetADCSample(iBin);
      waveforms[i]->SetBinContent(iBin + 1, adc);
    }

    Channel channel(waveforms[i], i);
    channels.push_back(channel);
  }

  mEvent->addChannels(channels);
  mEvent->setID(eventId);
  mEvent->setTriggerTime(triggerTime);
  eventId++;
  // Cleanup the information for this event.
  dataContainer->CleanupEvent();

  return 0;
}

/**
 * Retrieve the Waveform at a given index.
 *
 * Calls #getNextEvent() and increments `currentIndex` up to `index`. Returns
 * waveform at this index from the Midas file.
 *
 * Do not use this method if you are also using a Reader to read through the
 * file as the currentIndex will not be incremented properly.
 *
 * If using multi-channel data, this will get the waveform from the last
 * channel. In cases where this method will only be called once, it can be
 * replaced by creating a Reader with the specific DataFileFactory class and
 * calling Reader#next() the desires amount of times.
 */
Waveform *V1740File::getWaveform(int index) {
  while (index > currentIndex) {
    getNextEvent();
    currentIndex++;
  }
  if (index < currentIndex)
    std::cerr << "Index too low. Cannot read backward for V1740" << std::endl;
  return waveforms.back();
}

/**
 * Create and return an iterator (Reader) for V1740.
 *
 * This allows the RunManager to read through the events in the Midas file.
 */
std::unique_ptr<Reader> V1740File::createIterator() {
  return std::make_unique<Reader>(this);
}
