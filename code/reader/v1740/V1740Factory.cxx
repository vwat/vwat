/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "V1740Factory.h"

/**
 * Create and return a V1740 file.
 *
 * The V1740 file returned from this method is used to create an iterator
 * (Reader) to read through the file and its events.
 *
 * The `src` parameter contains the data required to create the V1740File.
 */
DataFile *V1740Factory::createDataFile(DataSource src,
                                       std::shared_ptr<Event> event) const {
  std::shared_ptr<LoLXEvent> lolxEvent =
      std::dynamic_pointer_cast<LoLXEvent>(event);
  return new V1740File(src, lolxEvent);
}
