/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATA_FILE_H
#define DATA_FILE_H

#include "../data_structures/Event.h"
#include "../data_structures/Waveform.h"
#include <jsoncpp/json/json.h>
#include <time.h>
#include <vector>

class Reader;

/**
 * Data from the configuration file that will be used with the DataFile and its
 * sub classes.
 */
struct DataSource {
  std::string device;      /*!< The type of device used in the experiment. */
  std::string filePath;    /*!< The path to the data file. */
  std::string outFilePath; /*!< The path and filename for the output file. */
  int channels;     /*!< The number of channels used in the experiment. */
  Json::Value vars; /*!< Additional variables for specific implementations. */
  std::string eventType;
};

/**
 * Data File interface for specific device implementations to inherit from.
 * Sub classes must implement the #createIterator() and #getNextEvent()
 * methods to be used in the RunManager.
 */
class DataFile {
protected:
  int numBins;   /*!< The number of bins for the baseline Histogram. */
  double ampMax; /*!< The maximum amplitude of the Waveforms. */
  double ampMin; /*!< The minimum amplitude of the Waveforms. */
  time_t mFileCreationTime; /*!< The creation time of the file. */

public:
  /**
   * DataFile constructor.
   *
   * The number of bins is initially set to -1. This will be modified if the
   * data file contains information about the Y scale of the waveforms.
   */
  DataFile() : numBins(-1) {}

  /**
   * DataFile destructor.
   */
  virtual ~DataFile() {}

  // Interface to be implemented by subclasses

  /**
   * Retrieve the Waveform at a given index.
   *
   * Implemented by the sub classes.
   * If using multi-channel data, this will get the waveform from the last
   * channel. In cases where this method will only be called once, it can be
   * replaced by creating a Reader with the specific DataFileFactory class and
   * calling Reader#next() the desires amount of times.
   */
  virtual Waveform *getWaveform(int index) = 0;

  /**
   * Create and return the next Event.
   *
   * Implemented by the sub classes.
   *
   * Each Event should contain a vector of channels, an ID and a trigger time.
   * Each channel should contain a single Waveform. This method is used in
   * Reader, to iterate over the data.
   *
   * If a device supports multiple channels, the number of channels should be
   * set in the constructor, and a Channel with a Waveform should be added to
   * the vector for each of the channels.
   *
   * See V1730File#getNextEvent() for an example of a multi-channel
   * implementation.
   */
  virtual int getNextEvent() = 0;

  /**
   * Create and return an iterator (Reader) for the DataFile.
   *
   * Implemented by the sub classes.
   *
   * In all implementations, the contents of this method should be '`return
   * std::make_unique<%Reader>(this);`'
   */
  virtual std::unique_ptr<Reader> createIterator() = 0;

  /**
   * Check if the Y limits are known.
   *
   * Returns `true` if the information about the Y scale of the Waveforms is
   * stored in the data. If it is, `numBins`, `ampMax` and `ampMin` should be
   * set in the constructor for the specific DataFile type.
   */
  bool yLimitsKnown() { return numBins > 0; }

  /**
   * Return the maximum amplitude for the Waveforms.
   * This method should only be called if #yLimitsKnown() has returned `true`.
   */
  double getAmpMax() { return ampMax; }

  /**
   * Return the minimum amplitude for the Waveforms.
   * This method should only be called if #yLimitsKnown() has returned `true`.
   */
  double getAmpMin() { return ampMin; }

  /**
   * Return the number of bins for the baseline Histogram.
   * This will return -1 unless the information about the Y scale of the
   * Waveforms is stored in the data.
   */
  int getNumBins() { return numBins; }

  /**
   * Return the file creation time.
   * This is currently not used, but may be required for certain implementation
   * such as `LecroyHdfFile`.
   */
  time_t getFileCreationTime() { return mFileCreationTime; }
};

#endif // DATA_FILE_H
