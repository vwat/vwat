/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LoLXEvent.h"

/**
 * LoLXEvent constructor.
 *
 * Calls the Event constructor using aChannels, aEventID and aTriggerTime.
 */
LoLXEvent::LoLXEvent(Channel::Channels aChannels, int aEventID,
                     double aTriggerTime)
    : Event(aChannels, aEventID, aTriggerTime) {}

/**
 * Returns the `multiplicity`
 *
 * Number of photons detected
 */
int LoLXEvent::getMultiplicity() { return multiplicity; };

/**
 * Sets the `multiplicity`
 *
 * Number of photons detected
 */
void LoLXEvent::setMultiplicity(int multiplicity) {
  this->multiplicity = multiplicity;
};

/**
 * Returns the `firstPulseTime`
 *
 * The time of the first photon detected
 */
double LoLXEvent::getFirstPulseTime() { return firstPulseTime; };

/**
 * Sets the `firstPulseTime`
 *
 * The time of the first photon detected
 */
void LoLXEvent::setFirstPulseTime(double firstPulseTime) {
  this->firstPulseTime = firstPulseTime;
};