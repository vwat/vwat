/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHANNEL_H
#define CHANNEL_H

#include "Pulse.h"
#include "Waveform.h"
#include <vector>

/**
 * A channel class that stores data pertaining to a particular channel.
 *
 * The Channel class will store the Waveform of the Channel and it's pulses in a
 * vector. A vector of Channel will be stored inside an Event class and can be
 * called whenever necessary.
 */
class Channel {
public:
  typedef std::vector<Channel> Channels;

  Channel(Waveform *newWaveform, int chID);
  ~Channel();

  void setPulses(std::vector<Pulse> pulses);
  Waveform *getWaveform();
  void setWaveform(Waveform *wf);
  std::vector<Pulse> *getPulses();
  bool getValid();
  void setValid(bool valid = true);
  int getChID();

private:
  Waveform *wf; /*!< Waveform of the current channel. */
  bool isValid;
  std::vector<Pulse>
      pulses; /*!< Pulses discovered of the current channel's waveform. */
  double triggerTime; /*!< Trigger time of the channel. */
  int chID;
};

#endif // CHANNEL_H
