/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Waveform.h"

/**
 * Waveform Constructor.
 *
 * Constructs a Waveform by creating a new TH1D object using `aNBin`, `aMin` and
 * `aMax`. Sets the name to `aName` if it is provided, or else constructs it.
 */
Waveform::Waveform(int aNBin, double aMin, double aMax, const char *aName)
    : TH1D("HWF", "", aNBin, aMin, aMax) {
  if (aName) {
    SetName(aName);
  } else {
    int ti = 0;
    char tName[50];
    sprintf(tName, "HWF%i", ti);
    while (gDirectory->Get(tName)) {
      ti++;
      sprintf(tName, "HWF%i", ti);
    }
    SetName(tName);
  }
}

/**
 * Waveform Constructor Overload.
 *
 * Constructs a Waveform by creating a new TH1D object using the TH1D passed as
 * a parameter, `aHisto`. Sets the name to aName if it is provided, or else
 * constructs it.
 */
Waveform::Waveform(const TH1D &aHisto, const char *aName) : TH1D(aHisto) {
  if (aName) {
    SetName(aName);
  } else {
    char tName[50];
    sprintf(tName, "%sWF", aHisto.GetName());
    SetName(tName);
  }
}

/**
 * Waveform Destructor.
 */
Waveform::~Waveform() {}

/**
 * Returns the Waveform's global Baseline.
 *
 * Returns the member variable `waveformBaseline`, a Baseline object.
 */
Baseline Waveform::getWaveformBaseline() { return waveformBaseline; }

/**
 * Sets the Waveform's global Baseline.
 *
 * Takes a Baseline as a parameter and sets the member variable,
 * `waveformBaseline`.
 */
void Waveform::setWaveformBaseline(Baseline baseline) {
  waveformBaseline = baseline;
}