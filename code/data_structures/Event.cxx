/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Event.h"
#include "Channel.h"

/**
 * Default constructor with no parameters.
 *
 * Creates an Event and sets eventID to 0.
 */
Event::Event() { eventID = 0; }

/**
 * Event constructor with parameters for channels, eventID and triggerTime.
 *
 * Creates an Event and sets the member variables using the parameters.
 */
Event::Event(Channel::Channels aChannels, int aEventID, double aTriggerTime)
    : channels(aChannels), eventID(aEventID), triggerTime(aTriggerTime) {}

/**
 * Store all the channels of the current event.
 *
 * Takes a vector of Channel as the parameter and sets the member vector,
 * `channels`.
 */
void Event::addChannels(Channel::Channels newChannels) {
  channels = newChannels;
}

/**
 * Return all the channels of the current event.
 *
 * Returns member vector, `channels` as an address reference when called.
 * This allows pass by reference and changes to the channels outside of the
 * Event class will persist.
 */
Channel::Channels *Event::getChannels() { return &channels; }

/**
 * Returns the events id 'eventID'
 *
 * Used for keeping track of event id when debugging pulse anal
 */
int Event::getID() { return eventID; }

void Event::setID(int id) { eventID = id; }

/**
 * Set the trigger time for the Event.
 *
 * Takes a double as a parameter and sets the member variable, `triggerTime`.
 */
void Event::setTriggerTime(double eventTriggerTime) {
  triggerTime = eventTriggerTime;
}

/**
 * Return the Event's trigger time.
 *
 * Returns the member variable, `triggerTime` when called.
 */
double Event::getTriggerTime() { return triggerTime; }
