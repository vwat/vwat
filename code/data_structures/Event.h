/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENT_H
#define EVENT_H

#include "Channel.h"
#include <vector>

/**
 * An Event class that stores all the channels related to the event.
 *
 * The Event class will store all the channels that associate with itself inside
 * a vector of Channels. It allows access to those channels via a getter method.
 */
class Event {
public:
  Event();
  Event(Channel::Channels aChannels, int aEventID, double aTriggerTime);
  virtual ~Event(){};

  void addChannels(std::vector<Channel> newChannels);
  std::vector<Channel> *getChannels();
  int getID();
  void setID(int id);
  void setTriggerTime(double eventTriggerTime);
  double getTriggerTime();

private:
  Channel::Channels channels; /*!< Channels of the current Event. */
  int eventID;                /*!< The ID of the Event, zero-indexed. */
  double triggerTime;         /*!< Trigger time of the Event. */
};

#endif // EVENT_H