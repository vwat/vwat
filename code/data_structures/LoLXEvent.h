/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOLX_EVENT_H
#define LOLX_EVENT_H

#include "Event.h"

/**
 * A LoLX specific Event class that contains additional variables from analysis.
 *
 * This class inherits from Event, and includes multiplicity and first pulse
 * time.
 */
class LoLXEvent : public Event {
public:
  LoLXEvent(Channel::Channels aChannels, int aEventID, double aTriggerTime);
  LoLXEvent(){};
  int getMultiplicity();
  double getFirstPulseTime();
  void setMultiplicity(int multiplicity);
  void setFirstPulseTime(double firstPulseTime);

private:
  int multiplicity;      /*!< Number of photons in an event */
  double firstPulseTime; /*!< Time of the first pulse in an Event. */
};

#endif // LOLX_EVENT_H