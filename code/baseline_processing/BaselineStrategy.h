/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __BASELINESTRATEGY_H
#define __BASELINESTRATEGY_H

#include "../data_structures/Channel.h"
#include <jsoncpp/json/json.h>

/**
 * Baseline extraction Strategy to create different Strategies for extracting
 * baselines from waveforms
 *
 * BaselineStrategy allows the RunManager to call in n number of baseline
 * strategies while keeping all the complexity hidden behind their
 * respective classes.
 *
 * This interface is purposely left quite broad to accommodate different types
 * of baselne extractions. The boolean return value (by convention) currently
 * indicates if a Channel was ruled out for analysis, but this should be easy to
 * adapt to a more descriptive data type if necessary. Refer to the caller
 * (BaselineProcessor::processEvent()) to see how it's currently used.
 */
class IBaselineStrategy {
public:
  virtual bool process(Channel &channel, Json::Value cfg) = 0;
  virtual std::vector<std::string> checkParams(Json::Value params) = 0;
  virtual ~IBaselineStrategy(){};
};

#endif // __BASELINESTRATEGY_H
