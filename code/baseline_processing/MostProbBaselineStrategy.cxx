/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MostProbBaselineStrategy.h"
#include "TROOT.h"

/*
 * MostProbBaselineStrategy constructor
 */
MostProbBaselineStrategy::MostProbBaselineStrategy() {}

/*
 * Unpacks the 'channel's Waveform and sets its waveform baseline value.
 * Return not implemented
 */
bool MostProbBaselineStrategy::process(Channel &channel, Json::Value cfg) {
  Baseline bs{};
  Waveform *wf = channel.getWaveform();

  calculateBaselineMu(*wf, bs, cfg);

  wf->setWaveformBaseline(bs);
  return true;
}

/**
 * Calculate the baseline 'mu' value for a given Waveform (in this case the
 * mode).
 *
 * Used to set a baseline in the #Pulse Finding algorithm that potential Pulses
 * can be compared against
 *
 * Return has not been implemented (not yet needed)
 */
double MostProbBaselineStrategy::calculateBaselineMu(Waveform &wf, Baseline &bs,
                                                     Json::Value cfg) {
  int numBinsConfig = cfg["NumBins"].asInt();
  double ampMax = cfg["MaxAmplitude"].asDouble();
  double ampMin = cfg["MinAmplitude"].asDouble();
  double wfAmpBinning = cfg["WaveformAmplitudeBinning"].asDouble();
  double noise = cfg["Noise"].asDouble();
  double rmsCutNoiseCoefficient = cfg["RMSCutNoiseCoefficient"].asDouble();

  const std::string baselineHistName =
      std::string{wf.GetName()} + std::string{"Baseline"};

  std::unique_ptr<TH1F> baselineHist;

  // initialize baseline histogram ROOT object if none exists
  baselineHist = std::unique_ptr<TH1F>(
      (TH1F *)gROOT->FindObjectAny(baselineHistName.c_str()));
  if (baselineHist) {
    baselineHist->Reset("C");
  } else {
    int numBinsBaseline = (ampMax - ampMin) / wfAmpBinning;
    numBinsBaseline = numBinsConfig ? numBinsConfig : numBinsBaseline;

    baselineHist = std::make_unique<TH1F>(baselineHistName.c_str(),
                                          baselineHistName.c_str(),
                                          numBinsBaseline, ampMin, ampMax);
  }

  // baseline histogram represents the distribution of amplitude values
  // across the waveform
  for (int bin = 1; bin <= wf.GetNbinsX(); bin++) {
    baselineHist->Fill(wf.GetBinContent(bin));
  }

  // calculate baseline mu value
  // TODO: this should be able to be replaced by get maximum but that doesn't
  // seem to work with this version of root (v6.22)
  int blMostProbBin = 0;
  int blMostProbCont = 0;
  for (int iBin = 1; iBin <= baselineHist->GetNbinsX(); iBin++) {
    if (baselineHist->GetBinContent(iBin) > blMostProbCont) {
      blMostProbCont = baselineHist->GetBinContent(iBin);
      blMostProbBin = iBin;
    }
  }

  double baselineMu = baselineHist->GetBinLowEdge(blMostProbBin);
  double baselineRMS = baselineHist->GetRMS();

  if (baselineRMS >= noise * rmsCutNoiseCoefficient) {
    return 0.0;
  }

  bs.mu = baselineMu;
  bs.RMS = baselineRMS;

  return bs.mu; // TODO: ask where failure state should be
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string>
MostProbBaselineStrategy::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {
      "NumBins",      "MaxAmplitude",
      "MinAmplitude", "WaveformAmplitudeBinning",
      "Noise",        "RMSCutNoiseCoefficient"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
