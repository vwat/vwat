/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PULSEANALYSISSTRATEGY_H
#define __PULSEANALYSISSTRATEGY_H

#include "../data_structures/Channel.h"
#include <jsoncpp/json/json.h>

/**
 * PulseAnalysis Strategy to create different Strategies for fitting Pulses
 *
 * PulseAnalysisStrategy allows the RunManager to call in n number of Pulse
 * Analysis Strategies while keeping all the complexity hidden behind their
 * respective classes.
 *
 * This interface is purposely left quite broad to accommodate different types
 * of analysis. The boolean return value (by convention) currently indicates if
 * a Channel was rules out for analysis, but this should be easy to adapt to a
 * more descriptive data type if necessary. Refer to the caller
 * (PulseAnalysisProcessor::processEvent()) to see how it's currently used.
 */
class IPulseAnalysisStrategy {
public:
  virtual bool analyze(Channel &channel, Json::Value cfg) = 0;
  virtual std::vector<std::string> checkParams(Json::Value params) = 0;
  virtual ~IPulseAnalysisStrategy() {}
  virtual std::string getFitFunctionName(int chID) = 0;
  virtual void setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) = 0;
  virtual TF1 *createFitFunction(Json::Value cfg, int chID) = 0;
  virtual void packPulseRefit(Pulse *pulse, TF1 *fitFunction, int fitPulseIndex,
                              double fitLowLimit, double fitUppLimit,
                              Json::Value cfg) = 0;
};

#endif // __PULSEANALYSISSTRATEGY_H
