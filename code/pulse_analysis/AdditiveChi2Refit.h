/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ADDITIVECHI2REFIT_H_
#define __ADDITIVECHI2REFIT_H_

#include "../data_structures/Pulse.h"
#include "PulseAnalysisStrategy.h"
#include "TF1.h"
#include <jsoncpp/json/json.h>
#include <vector>

/**
 * A structure that provides indices to a single pulse group and the relevant
 * information of the group so that they can all be fit together as
 * superpositions
 */
struct PulseGroup {
  double lowLimit;
  double uppLimit;
  double refitChi2;
  double refitNDF;
  std::vector<uint32_t> pulseGroup;
};

/**
 * A refactor of the original fitting strategy. In broad terms there are 2
 * processes. First the incoming channel has marked positions of where pulses
 * might be, a fitting function is used to fit the waveform in the region
 * identifies by the pulse finder The newly fit pulses then undergo reffiting
 * which progressively add more pulses in the region to account for those missed
 * by the pulse finder. See 'analyze' and 'refit' in
 * Exp2GausPulseFittingStrategy.cxx for more detail. The individual fitting
 * parameters are defined by there child classes
 */
class IAdditiveChi2Refit : public IPulseAnalysisStrategy {
public:
  /**
   * Static method ensure that newly added pulses are pushed both the outgoing
   * pulse vector, but also into there respective pulse group
   */
  static void pushPulse(std::vector<Pulse> *pulses, PulseGroup *pulseGroup,
                        Pulse newPulse) {
    pulses->push_back(newPulse);
    pulseGroup->pulseGroup.push_back(pulses->size() - 1);
  }
  bool analyze(Channel &channel, Json::Value cfg) override;
  std::vector<std::string> checkParams(Json::Value params) override;
  virtual std::string getFitFunctionName(int chID) override;
  virtual TF1 *createFitFunction(Json::Value cfg, int chID) override;
  virtual void packPulseRefit(Pulse *pulse, TF1 *fitFunction, int fitPulseIndex,
                              double fitLowLimit, double fitUppLimit,
                              Json::Value cfg) override;
  virtual int getNumConstParams();

protected:
  virtual void unpackPulseFit(Pulse *pulse, TF1 *fitFunction,
                              int fitPulseIndex);
  virtual void packPulseFit(Pulse *pulse, TF1 *fitFunction, int fitPulseIndex,
                            double fitLowLimit, double fitUppLimit,
                            Json::Value cfg);

  void sortPulses(Channel *channel);
  bool refit(Channel *channel, TF1 *fitFunction,
             std::vector<PulseGroup> &pulseGroups, Json::Value cfg);
  void calcSinglePulseTemplateChi2(Channel *channel, Json::Value cfg);
  bool checkPulses(Channel &channel, Json::Value cfg);
  std::string fitOption;
};

#endif // __ADDITIVECHI2REFIT_H_