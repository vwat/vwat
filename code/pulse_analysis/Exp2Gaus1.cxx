/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Exp2Gaus1.h"

/**
 * Sets the paremeters on a fitting function for Pulse Analysis.
 * Take in a pointer to a fitting function 'fit'
 * And sets the apropirate parameters of the function for the fitting object
 * This helper method should be called int pulse analysis
 * code before fitting is executed.
 */
void Exp2Gaus1::setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();

  fit->ReleaseParameter(0);
  fit->SetParameter(0, p.baseline);

  fit->FixParameter(1, riseTimeSigma);
  fit->FixParameter(2, fallTimeTau);
  fit->FixParameter(3, time2Frac);
  fit->FixParameter(4, fallTime2Tau);
}