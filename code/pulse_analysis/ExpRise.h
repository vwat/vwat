/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EXPRISE_H_
#define __EXPRISE_H_

#include "AdditiveChi2Refit.h"

/**
 * An PulseFittingStrategy1 implementation used to analyze and fit Pulses in a
 * Waveform.
 *
 * Overides the PulseFittingStrategy1's 'setFitParemeters' to allow all the
 * parameters to be fit
 */
class ExpRise : public IAdditiveChi2Refit {

public:
  void setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) override;
  std::string getFitFunctionName(int chID) override;
  TF1 *createFitFunction(Json::Value cfg, int chID) override;
};

class ExpRiseFloating : public ExpRise {

public:
  void setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) override;
};

#endif // __EXPRISE_H_
