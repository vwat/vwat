/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CR_RC.h"
#include "../WaveformStatistic.h"
#include "TROOT.h"

/*
 * Sets the pulse strategies fitting parameters
 * p0: baseline
 * p1: std of the guass (riseTimeSigma)
 * p2: fallTimeTau
 * p3: inverse time constant of overshoot (time2Frac) left as inverse to get
 * resonable numbers p4: fallTime2Tau p6: baselineShift shift in the baseline
 * before and after the pulse
 */
void CR_RC::setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asInt();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asInt();
  double baselineShift = cfg["BaselineShift"].asDouble();
  double noise = cfg["Noise"].asDouble();

  fit->ReleaseParameter(0);
  fit->SetParameter(0, p.baseline);
  fit->SetParLimits(0, p.baseline - noise * 3, p.baseline + noise * 3);

  fit->FixParameter(1, riseTimeSigma);
  // fit->SetParLimits(1, riseTimeSigma*0.50,riseTimeSigma*1.50);

  fit->FixParameter(2, fallTimeTau);
  // fit->SetParLimits(2, fallTimeTau*0.50,fallTimeTau*1.50);

  fit->FixParameter(3, time2Frac);
  // fit->SetParLimits(3, 0,0.08);

  fit->FixParameter(4, fallTime2Tau);
  // fit->SetParLimits(4, fallTime2Tau*0.50,fallTime2Tau*1.50);

  fit->FixParameter(6, baselineShift); // baselineShift
  // fit->SetParLimits(6, 0.0,baselineShift*1.10);
}

/**
 * Returns the name of the root fitting function
 */
std::string CR_RC::getFitFunctionName(int chID) {
  return "FCR_RC" + std::to_string(chID);
}

/**
 * Pulls the pulse information out of the fitting fitunction `fitFunction` and
 * puts it into the `pusle`. Called during pulse fitting and refitting
 */
void CR_RC::unpackPulseFit(Pulse *pulse, TF1 *fitFunction, int fitPulseIndex) {
  int basicParams = this->getNumConstParams();
  pulse->fit.amp = fitFunction->GetParameter(basicParams + 2 * fitPulseIndex);
  pulse->fit.time =
      fitFunction->GetParameter(basicParams + 1 + 2 * fitPulseIndex);

  pulse->fit.baseline = fitFunction->GetParameter(0);
  pulse->fit.riseTime = fitFunction->GetParameter(1);
  pulse->fit.fallTime = fitFunction->GetParameter(2);
  pulse->fit.time2Frac = fitFunction->GetParameter(3);
  pulse->fit.fallTime2 = fitFunction->GetParameter(4);
  // pulse->fit.baselineShift = fitFunction->GetParameter(6); TODO: baseline
  // shift might not be needed, but could be useful
  pulse->fit.chi2 = fitFunction->GetChisquare();
  pulse->fit.NDF = fitFunction->GetNDF();
  pulse->fit.charge =
      fitFunction->Integral(pulse->fit.lowLimit, pulse->fit.highLimit) -
      (pulse->fit.highLimit - pulse->fit.lowLimit) *
          pulse->fit.baseline; // TODO: integral is the same for each
                               // pulse in the pulse group
}

/**
 * Puts the pulse information from `pulse` into the fitting function
 * `fitFunction`, called during inital fitting of a pulse group
 */
void CR_RC::packPulseRefit(Pulse *pulse, TF1 *fitFunction, int fitPulseIndex,
                           double fitLowLimit, double fitUppLimit,
                           Json::Value cfg) {
  int basicParams = this->getNumConstParams();
  if (pulse) {
    int polarity = cfg["Polarity"].asInt();
    bool floating = cfg["isFloating"].asBool();

    fitFunction->ReleaseParameter(basicParams + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 2 * fitPulseIndex, pulse->fit.amp);

    if (floating) {
      fitFunction->SetParameter(0, pulse->fit.baseline);
      fitFunction->SetParameter(1, pulse->fit.riseTime);
      fitFunction->SetParameter(2, pulse->fit.fallTime);
      fitFunction->SetParameter(3, pulse->fit.time2Frac);
      fitFunction->SetParameter(4, pulse->fit.fallTime2);
    }

    if (polarity > 0)
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, 0, 1e10);
    else
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, -1e10, 0);

    fitFunction->ReleaseParameter(basicParams + 1 + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 1 + 2 * fitPulseIndex,
                              pulse->fit.time);
    fitFunction->SetParLimits(basicParams + 1 + 2 * fitPulseIndex, fitLowLimit,
                              fitUppLimit);

  } else {

    fitFunction->FixParameter(basicParams + 2 * (fitPulseIndex), 0);
    fitFunction->FixParameter(basicParams + +1 + 2 * (fitPulseIndex), 0);
  }
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string> CR_RC::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"isFloating", "Polarity",
                                             "BaselineShift"};
  std::vector<std::string> missingParams =
      this->IAdditiveChi2Refit::checkParams(params);
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}

/**
 * Create the cr-rc root fit  function for %Pulse Analysis.
 */
TF1 *CR_RC::createFitFunction(Json::Value cfg, int chID) {
  TString fit_name(getFitFunctionName(chID));
  int mNFitPulseMax = cfg["NumPulsesFit"].asInt();
  TF1 *fitFunction = (TF1 *)gROOT->FindObject(fit_name);
  if (fitFunction && gROOT->FindObjectAny(fit_name)) {
    // fitFunction->Delete();
  } else {
    fitFunction =
        new TF1(fit_name, FuncCR_RCMulti, 0., 0.5e-6, 7 + mNFitPulseMax * 2);
    // fitFunction->SetParLimits(1, 0., 1e-6); // sigma
    // fitFunction->SetParLimits(2, 0., 1e-6); // tau
  }
  return fitFunction;
}

/**
 * Returns the number of parmaters that defines the shape of the fitting
 * function
 */
int CR_RC::getNumConstParams() { return 7; }