# Macros

# EventStepperViewer

## Usage

``` bash
root -l
.x macros/EventStepperViewer.C("Experiment_Path", Event_Number, Displayed_Channel_ID, Horizontal_Channel_Displayed)
```

### Experiment Path

The path to the experiment config file to run.

### Event Number

The event number to start viewing.

### Displayed Channel ID

The ID of channels to display (zero indexed). Set to -1 to view all available channels.

### Horizontal Channel Displayed

The number of columns of channel to display.

## GUI Tips

To view a single channel `X`, use view drop down menu and select `chX`. To return, select `All` from the drop down menu.
