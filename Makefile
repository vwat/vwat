MIDASLIBS = $(MIDASSYS)/lib/libmidas.a
ROOTANALIBS = $(ROOTANASYS)/lib/librootana.a
ROOTANASLIBS = $(ROOTANASYS)/lib/librootana.so
ROOTANADIR = $(ROOTANASYS)/include

GFLAGS = -g -O0
CFLAGS = -c -fPIC $(GFLAGS) $(shell root-config --cflags) -I. -I$(MIDASSYS)/include -I$(ROOTANASYS)/include -std=c++14
LFLAGS = $(GFLAGS) $(shell root-config --libs) $(ROOTANALIBS) $(MIDASLIBS) -lMinuit -lz -lm
LGUIFLAGS = $(GFLAGS) $(shell root-config --glibs) $(ROOTANALIBS) $(MIDASLIBS) -lMinuit -lz -lm

COMMON_OBJ = obj/RunManager.o obj/reader/v1730/V1730Factory.o obj/reader/Reader.o obj/reader/proto0_laser/Proto0LaserFactory.o \
obj/reader/v1730/V1730File.o obj/reader/v1740/TV1740Handler.o obj/reader/v1740/V1740Factory.o obj/reader/lngs_wav/LngsWavFactory.o obj/reader/synthesizer/ExpSynthesizerFactory.o \
obj/reader/v1740/TV1740Data.o obj/reader/v1740/V1740File.o obj/reader/lngs_wav/LngsWavFile.o obj/reader/proto0_laser/Proto0LaserFile.o obj/reader/synthesizer/ExpSynthesizerFile.o \
obj/writer/PulseWriteProcessor.o obj/writer/NTupleWriter.o obj/writer/TTreeWriter.o obj/writer/TTreeBranch.o obj/writer/LoLXTTreeWriter.o \
obj/data_structures/Waveform.o obj/data_structures/Event.o obj/data_structures/LoLXEvent.o obj/data_structures/Channel.o obj/WaveformStatistic.o obj/pulse_finding/PFS.o \
obj/pulse_finding/PFSProminence.o obj/pulse_finding/PFSAvergeBinThreshold.o obj/pulse_finding/PFSWidth.o obj/pulse_finding/PFSPolarity.o obj/pulse_finding/PulseFindingProcessor.o obj/pulse_analysis/AdditiveChi2Refit.o \
obj/pulse_analysis/Exp2Gaus1.o obj/pulse_analysis/Exp2Gaus2.o obj/pulse_analysis/Exp2Gaus3.o obj/pulse_analysis/ExpRise.o obj/pulse_analysis/CR_RC.o \
obj/pulse_analysis/PulseAnalysisProcessor.o obj/StepRunManager.o obj/baseline_processing/BaselineProcessor.o \
obj/filtering/TriangleFilter.o obj/filtering/ExpFilter.o obj/filtering/TrapezoidalFilter.o obj/filtering/DeConvFilter.o obj/filtering/ConstantFractionFilter.o obj/filtering/MeanFilter.o obj/filtering/MatchedFilter.o \
obj/filtering/ConvolutionFilterStrategy.o obj/filtering/FilterProcessor.o obj/baseline_processing/MostProbBaselineStrategy.o \
obj/event_analysis/LoLXEventProcessor.o obj/filtering/ExpMatchFilter.o obj/event_analysis/EventEfficiency.o obj/filtering/Exp2GausMatchedFilter.o

all: lib bin/vanwftk bin/EventViewer

PF : bin/PulseFinding.exe

lib : lib/libSipmAnalysis.so

lib/libSipmAnalysis.so : $(COMMON_OBJ) obj/RootDict.o
	mkdir -p $(@D)
	$(CXX) -shared -fPIC -o lib/libSipmAnalysis.so $^ $(LFLAGS) -lsndfile
	@cp code/RootDict_rdict.pcm lib

bin/EventViewer : obj/EventViewer.o $(COMMON_OBJ) obj/GUIDict.o
	mkdir -p $(@D)
	$(CXX) -o bin/EventViewer $^ $(LGUIFLAGS) -ljsoncpp -lsndfile

bin/vanwftk : obj/Main.o $(COMMON_OBJ) obj/VanwftkDict.o
	mkdir -p $(@D)
	$(CXX) -o bin/vanwftk $^ $(LFLAGS) -ljsoncpp -lsndfile

obj/VanwftkDict.o : code/data_structures/Baseline.h code/data_structures/Channel.h code/data_structures/Event.h \
code/data_structures/Waveform.h code/data_structures/FitPulse.h code/data_structures/Pulse.h code/RunManager.h \
code/writer/TTreeBranch.h code/Linkdef.h
	mkdir -p bin
	rootcling -f bin/VanwftkDict.cxx -c -I$(ROOTANADIR) $^
	mkdir -p $(@D)
	$(CXX) -c -o $@ bin/VanwftkDict.cxx $(CFLAGS)

obj/RootDict.o : code/data_structures/Baseline.h code/data_structures/Channel.h code/data_structures/Event.h \
code/data_structures/Waveform.h code/data_structures/FitPulse.h code/data_structures/Pulse.h code/RunManager.h \
code/StepRunManager.h code/writer/TTreeBranch.h
	# rootcint -f code/RootDict.cxx -c -I/home/deap/nEXO/packages/hdf5-1.8.13/hdf5/include -I$ROOTANADIR $^
	rootcint -f code/RootDict.cxx -c -I$(ROOTANADIR) $^
	mkdir -p $(@D)
	$(CXX) -c -o $@ code/RootDict.cxx $(CFLAGS)

obj/GUIDict.o : code/data_structures/Baseline.h code/data_structures/Channel.h code/data_structures/Event.h \
code/data_structures/Waveform.h code/data_structures/FitPulse.h code/data_structures/Pulse.h code/RunManager.h \
code/writer/TTreeBranch.h code/EventViewer.h code/GUILinkdef.h
	mkdir -p bin
	rootcling -f bin/GUIDict.cxx -c -I$(ROOTANADIR) $^
	mkdir -p $(@D)
	$(CXX) -c -o $@ bin/GUIDict.cxx $(CFLAGS)

define build-obj
$(CC) $(CFLAGS) $< -o $@
endef

obj/%.o: code/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/%.o: code/reader/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/v1730/%.o: code/v1730/reader/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/synthesizer/%.o: code/reader/synthesizer/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/v1740/%.o: code/v1740/reader/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/lngs_wav/%.o: code/reader/lngs_wav/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/reader/proto0_laser/%.o: code/reader/proto0_laser/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/writer/%.o: code/writer/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/baseline_processing/%.o: code/baseline_processing/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/filtering/%.o: code/filtering/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/pulse_finding/%.o: code/pulse_finding/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/pulse_analysis/%.o: code/pulse_analysis/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/event_analysis/%.o: code/event_analysis/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

obj/data_structures/%.o: code/data_structures/%.cxx
	mkdir -p $(@D)
	$(CXX) -c -o $@ $< $(CFLAGS)
	$(build-obj)

format:
	find code -name '*.cxx' -or -name '*.h*' | xargs clang-format -i -Werror

lint:
	find code -name '*.cxx' -or -name '*.h*' | xargs clang-format --dry-run -Werror -ferror-limit=1

shellcheck:
	find scripts -name '*.sh' | xargs shellcheck

.PHONY: docs
docs:
	mkdir -p public
	doxygen Doxyfile

clean/docs:
	rm -rf public

clean:
	rm -rf obj bin lib
