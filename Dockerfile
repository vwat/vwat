FROM rootproject/root

RUN apt-get update && apt-get install -y git build-essential rsync vim gdb libjsoncpp-dev libsndfile-dev

RUN mkdir /home/vanwftk

WORKDIR /home/vanwftk

RUN git clone https://bitbucket.org/tmidas/midas --recursive

ENV MIDASSYS /home/vanwftk/midas

WORKDIR /home/vanwftk/midas
RUN make mini && \
    ln -s linux-x86_64/lib lib && \
    ln -s linux-x86_64/bin bin
WORKDIR /home/vanwftk

RUN git clone https://bitbucket.org/tmidas/rootana

ENV ROOTANASYS /home/vanwftk/rootana

WORKDIR /home/vanwftk/rootana
RUN git checkout rootana-2020-03-a && \
    sed -i 's/CXXFLAGS = -g -O2 -Wall -Wuninitialized -I\.\/include/CXXFLAGS = -g -O2 -Wall -Wuninitialized -I\.\/include -fPIC/g' /home/vanwftk/rootana/Makefile && \
    make

WORKDIR /home/vanwftk
COPY . vwat

WORKDIR /home/vanwftk/vwat

RUN make clean && make all

CMD ["/bin/bash"]
