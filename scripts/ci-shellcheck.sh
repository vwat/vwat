#!/bin/sh

make shellcheck
LINT_SUCCESS="$?"

[ "$LINT_SUCCESS" -ne 0 ] && printf "\n\nPipeline job failed: Shell scripts contain warnings/errors\n"

exit "$LINT_SUCCESS"
