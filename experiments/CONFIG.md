# Configuration File

Create a new configuration file for each experiment with the required values for the variables below.

# EventLimit: int

Maximum number of events to read from the dataset. If the EventLimit is greater than the number of events in the dataset, this is handled by the reader.

# DataSource

___

Information about the dataset for reading, processing and writing.

## FilePath: String

Path to the the dataset to be read.

## OutputFilePath: String (optional)

Path and filename for the output file. If not included, the output file will be named the same as the input file (with a different file extension), and saved to the same directory.

## Device: String

Digitizer of the dataset to be read.

Possible values: ```V1730, V1740, LNGS-WAV, PROTO0-LASER, EXP-SYNTH```

## Channels: int

Number of channels to read in the dataset.

Possible values: From 1 to the total number of channels used in the experiment.

## Variables (DataSource) : Object

Device specific variable if additional variables are needed.

Currently only needed for `PROTO0-LASER` to specify the `Branch` to read such as `"Branch": "SIPM00"`.

# ChannelConfigs

___

Specifies the channel pulses strategies to be used in the the analysis and any strategy specific variables.
If using multiple channels, either create a single object that will be repeated for all channels, or create an object for each channel individually.

## FilterStrategy: String

The strategy to filter waveforms.

Possible values: ```Triangle, Trapezoidal, DeConvFilter, ConstantFractionFilter, Exp, Mean, ExpMatch, Exp2GausMatch, NA```

## PulseFindingStrategy: String

The strategy to find pulses in a waveform.

Possible values: ```PFS, PFSWidth, PFSPolarity, PFSProminence, PFSProminenceAverage, LargestValue```

## PulseAnalysisStrategy: String

The strategy to analysis the found pulses, for example fitting the pulses.

Possible values: ```AdditiveChi2RefitExp2Gaus1 (Exp2Gaus1), AdditiveChi2RefitExp2Gaus2 (Exp2Gaus2), AdditiveChi2RefitExp2Gaus3 (Exp2Gaus3), AdditiveChi2RefitExpRise (ExpRise), AdditiveChi2RefitExpRiseFloating (ExpRiseFloating), AdditiveChi2RefitCR_RC (CR_RC)```

## BaselineStrategy: String

The strategy to extract the baseline from the waveform.

Possible values: ```MostProbable```

## WritingStrategy: String

The strategy to write the results of the analysis to file.

Possible values: ```TTreeWriter, NTupleWriter```

## EventProcessing: String

Processor to handle event level analysis.

Possible values: ```LOLX, Efficiency, NA```

## Variables (ChannelConfigs)

___

Array of objects for variables used in the algorithms. Each strategy required different config variables. Each heading describes which ones are needed for that specific strategy. [Variable List](#variablelist) contains all of the variables.

### Triangle (Filtering)

___

- [KernelSize](#KernelSize)

### Trapezoidal (Filtering)

___

- [KernelSize](#KernelSize)
- [l_delay](#l_delay)

### DeConvFilter (Filtering)

___

- [KernelSize](#KernelSize)
- [FallTimeTau](#FallTimeTau)

### ConstantFractionFilter (Filtering)

___

- [KernelSize](#KernelSize)
- [l_delay](#l_delay)
- [constant_filter_fraction](#constant_filter_fraction)

### Exp (Filtering)

___

- [SmoothingFactor](#SmoothingFactor)

### Mean (Filtering)

___

- [KernelSize](#KernelSize)

### ExpMatch (Filtering)

___

- [KernelSize](#KernelSize)
- [FallTimeTau](#FallTimeTau)

### Exp2GausMatch (Filtering)

___

- [KernelSize](#KernelSize)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [Time2Frac](#Time2Frac)
- [FallTime2Tau](#FallTime2Tau)

### MostProbable (Baseline Processing)

___

- [NumBins](#NumBins)
- [MaxAmplitude](#MaxAmplitude)
- [MinAmplitude](#MinAmplitude)
- [WaveformAmplitudeBinning](#WaveformAmplitudeBinning)
- [Noise](#Noise)
- [RMSCutNoiseCoefficient](#RMSCutNoiseCoefficient)

### PFS (Pulse Finding)

___

- [Noise](#Noise)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [Polarity](#Polarity)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [PulseBaselineCalcBeginOffset](#PulseBaselineCalcBeginOffset)
- [PulseBaselineCalcEndOffset](#PulseBaselineCalcEndOffset)
- [PulseChargeCalcOffset](#PulseChargeCalcOffset)
- [PulseStartCalcOffset](#PulseStartCalcOffset)
- [PulseStartNoiseThresholdCoefficient](#PulseStartNoiseThresholdCoefficient)
- [PulseEndNoiseThresholdCoefficient](#PulseEndNoiseThresholdCoefficient)
- [PulseChargeCutCoefficient](#PulseChargeCutCoefficient)
- [PulseBaselineDifferenceCutCoefficient](#PulseBaselineDifferenceCutCoefficient)

### PFSProminence (Pulse Finding)

___

- [Prominence](#Prominence)
- [WaveformStart](#WaveformStart)
- [WaveformEnd](#WaveformEnd)
- [RiseTimeSigma](#RiseTimeSigma)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [Noise](#Noise)
- [Polarity](#Polarity)

### PFSPolarity (Pulse Finding)

___

- [Noise](#Noise)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [Polarity](#Polarity)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [PulseBaselineCalcBeginOffset](#PulseBaselineCalcBeginOffset)
- [PulseBaselineCalcEndOffset](#PulseBaselineCalcEndOffset)
- [PulseChargeCalcOffset](#PulseChargeCalcOffset)
- [PulseStartCalcOffset](#PulseStartCalcOffset)
- [PulseStartNoiseThresholdCoefficient](#PulseStartNoiseThresholdCoefficient)
- [PulseEndNoiseThresholdCoefficient](#PulseEndNoiseThresholdCoefficient)
- [PulseChargeCutCoefficient](#PulseChargeCutCoefficient)
- [PulseOppositePolarityAmpCutCoefficient](#PulseOppositePolarityAmpCutCoefficient)
- [MinPulseWidth](#MinPulseWidth)
- [PulseBaselineDifferenceCutCoefficient](#PulseBaselineDifferenceCutCoefficient)
- [RiseTimeThreshold](#RiseTimeThreshold)
- [Prominence](#Prominence)

### PFSWidth (Pulse Finding)

___

- [Noise](#Noise)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [Polarity](#Polarity)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [PulseStartCalcOffset](#PulseStartCalcOffset)
- [PulseStartNoiseThresholdCoefficient](#PulseStartNoiseThresholdCoefficient)
- [PulseEndNoiseThresholdCoefficient](#PulseEndNoiseThresholdCoefficient)
- [MinPulseWidth](#MinPulseWidth)
- [Prominence](#Prominence)
- [WaveformStart](#WaveformStart)
- [WaveformEnd](#WaveformEnd)

### PFSProminenceAverage (Pulse Finding)

___

- [Noise](#Noise)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [Polarity](#Polarity)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [Prominence](#Prominence)
- [ThresholdWindowSize](#ThresholdWindowSize)
- [WaveformStart](#WaveformStart)
- [WaveformEnd](#WaveformEnd)

### LargestValue (Pulse Finding)

___

- [Polarity](#Polarity)

### AdditiveChi2Refitxxx (Pulse Analysis) {#AdditiveChi2Refitxxx}

___

- [MinChiSquareForRefit](#MinChiSquareForRefit)
- [PulseFitLowerBoundOffset](#PulseFitLowerBoundOffset)
- [PulseBaselineCalcEndOffset](#PulseBaselineCalcEndOffset)
- [RiseTimeSigmaUpperLimitCoefficient](#RiseTimeSigmaUpperLimitCoefficient)
- [TemplateCheck](#TemplateCheck)
- [MinTimeDifference](#MinTimeDifference)
- [NumPulsesFit](#NumPulsesFit)
- [doFit](#doFit)
- [refit](#refit)
- [Noise](#Noise)
- [FitOption](#FitOption)
- [RiseTimeSigma](#RiseTimeSigma)
- [FallTimeTau](#FallTimeTau)
- [FallTime2Tau](#FallTime2Tau)
- [Time2Frac](#Time2Frac)
- [Polarity](#Polarity)
- [RiseTimeSigmaCoefficient](#RiseTimeSigmaCoefficient)
- [FallTimeTauCoefficient](#FallTimeTauCoefficient)
- [FallTime2TauCoefficient](#FallTime2TauCoefficient)

### AdditiveChi2RefitCR_RC (Pulse Analysis)

___

Requires all variables for [AdditiveChi2Refitxxx](#AdditiveChi2Refitxxx) and:

- [BaselineShift](#BaselineShift)
- [isFloating](#isFloating)

### EventEfficiency (Event Processing)

___

- [isTrigger](#isTrigger)
- [WindowStart](#WindowStart)
- [WindowStop](#WindowStop)

### Variable List {#variablelist}

#### NumBins: int {#NumBins}

The number of bins for the baseline histogram. If set to `0` and not modified by the data file, the number is calculated with mAmpMax, mAmpMin and mWFAmpBining.

#### MaxAmplitude: double {#MaxAmplitude}

The maximum amplitude of the waveform when creating the baseline histogram for baseline extraction.

#### MinAmplitude: double {#MinAmplitude}

The minimum amplitude of the waveform when creating the baseline histogram for baseline extraction.

#### WaveformAmplitudeBinning: double {#WaveformAmplitudeBinning}

The binning of the waveforms amplitude, used to calculate the number of bins in the baseline histogram. Should be in units per bin.

#### Noise: double {#Noise}

The sigma of the background of the waveform.

#### RMSCutNoiseCoefficient: double {#RMSCutNoiseCoefficient}

Used in the baseline RMS cut. Cuts out waveforms with RMS values larger than the `noiseCoefficient` by the `noise`.

#### RiseTimeSigma: double {#RiseTimeSigma}

Rise time of the pulse, used both in the pulse finding to find the start of the pulse, and in pulse analysis to fit the pulses.

#### FallTimeTau: double {#FallTimeTau}

The time constant of the exponential fall-off of the pulse. Used in the pulse finding with the `FallTime2Tau` to calculate the end of the pulse. Also used in pulse fitting to fit the found pulses.

#### FallTime2Tau: double {#FallTime2Tau}

The second time constant of the exponential fall-off of the pulse. Used in the pulse finding with the `FallTimeTau` to calculate the end of the pulse. Also used in pulse fitting to fit the found pulses.

#### Time2Frac: double {#Time2Frac}

The mixing coefficient of the contribution of the 2 exponential falls

Possible values: `(0-1)`

#### Polarity: double {#Polarity}

The polarity of the waveform dataset.

#### RiseTimeSigmaCoefficient: double {#RiseTimeSigmaCoefficient}

Coefficient used in the calculation of the number of bins from the peak that the pulses starts. In pulses finding this dictates the lowest limit for the pulse. In the pulse analysis its used to set the initial condition of the pulse fitting.

#### FallTimeTauCoefficient: double {#FallTimeTauCoefficient}

Coefficient used in the calculation of the number of bins in the pulses tail. Used in the pulse finding to calculate the end of the pulse. Used in the pulse analysis to calculated the end of the pulse. The upper limit determines which pulses will be fit together.

#### FallTime2TauCoefficient: double {#FallTime2TauCoefficient}

Same use as `FallTimeTauCoefficient` but multiples the second fall time.

#### PulseBaselineDifferenceCutCoefficient: double {#PulseBaselineDifferenceCutCoefficient}

Controls the maximum distance allowed off of the baseline. Multiples the `noise` of the waveform, cutting pulses the have a local baseline that is `noise*pulseBaselineDifferenceCutCoefficient` father from the global waveforms baseline.

#### PulseChargeCutCoefficient: double {#PulseChargeCutCoefficient}

Controls the charge cut on the pulses. Pulses with a charge less than `noise*pulseChargeCutCoefficient*ApproxPulseArea` are cut.

#### PulseChargeCalcOffset: int {#PulseChargeCalcOffset}

Number of addition bins ahead of the pulse to start the integration of the pulses charge which will be used in the charge cut.

#### PulseStartNoiseThresholdCoefficient: double {#PulseStartNoiseThresholdCoefficient}

Used to calculate the point at which the pulse starts. Multiples the waveforms `noise` and look where the waveform drops bellow `PulseStartCalcNoiseCoefficient*noise`

#### PulseEndNoiseThresholdCoefficient: double {#PulseEndNoiseThresholdCoefficient}

Used to calculate the point at which the pulse ends. Multiples the waveforms `noise` and look where the waveform drops bellow `PulseEndCalcNoiseCoefficient*noise`

#### PulseStartCalcOffset: int {#PulseStartCalcOffset}

Number of addition bins ahead of the pulse when calculating a more accurate starting point of the pulse. The new starting point is used to approximate the pulse width.

#### PulseBaselineCalcBeginOffset: int {#PulseBaselineCalcBeginOffset}

Number of bins to look ahead to start the calculation of the pulses baseline. The pulses baseline is extracted from the distribution of the waveform on the bin interval of (`PulseBaselineCalcBeginOffset`, `PulseBaselineCalcEndOffset`)

#### PulseBaselineCalcEndOffset: int {#PulseBaselineCalcEndOffset}

Number of bins to look ahead to stop the calculation of the pulses baseline. The pulses baseline is extracted from the distribution of the waveform on the bin interval of (`PulseBaselineCalcBeginOffset`, `PulseBaselineCalcEndOffset`)

#### Prominence: double {#Prominence}

Multiplies the noise to generate a threshold at which a pulse is considered 'found' i.e (bin_value-baseline)*polarity > Prominence*noise

#### WaveformStart: int {#WaveformStart}

Number of bins to skip at the start of the waveform

#### WaveformEnd: int {#WaveformEnd}

Number of bins to skip at the end of the waveform

#### RiseTimeThreshold: int {#RiseTimeThreshold}

The minimum pulse width to count as a real pulse

#### MinPulseWidth: int {#MinPulseWidth}

The minimum pulse width to count as a real pulse

#### PulseOppositePolarityAmpCutCoefficient: double {#PulseOppositePolarityAmpCutCoefficient}

Multiplied by the amplitude of the proposed pulse to determine how far across the baseline the pulse has to swing to be discarded. Deals with large amplitude ringing.

#### MinChiSquareForRefit: double {#MinChiSquareForRefit}

The minimum chi2/NDF required to be refit. Any fits lower will not be refit.

#### PulseFitLowerBoundOffset: int {#PulseFitLowerBoundOffset}

The number of addition bins to add when calculation the fitting functions lower bound.

#### PulseBaselineCalcEndOffset: double {#PulseBaselineCalcEndOffset}

Multiplies the pulse rise time of the waveform when calculating the lower limit of the fitting function's peak time parameter.

#### RiseTimeSigmaUpperLimitCoefficient: double {#RiseTimeSigmaUpperLimitCoefficient}

Multiplies the pulse rise time of the waveform when calculating the upper limit of the fitting function's peak time parameter. The upper limit also determines what pulses will be fit together.

#### TemplateCheck: int {#TemplateCheck}

Toggles whether the template check is used, is **deprecated**?

Possible values: `0, 1`

#### MinTimeDifference: double {#MinTimeDifference}

The minimum time difference between pulses allowed during refitting.

#### NumPulsesFit: int {#NumPulsesFit}

Maximum number of pules allowed in refitting.

#### doFit: int {#doFit}

Toggles whether to use the fitting algorithm.

Possible values: `0, 1`

#### refit: int {#refit}

Toggles whether to use the refitting algorithm.

Possible values: `0, 1`

#### FitOption: String {#FitOption}

Options passed into the ROOT fitting algorithm during pulse fitting and refitting.

#### BaselineShift: double {#BaselineShift}

Shift in the baseline for pulse analysis (?).

#### isFloating: int {#isFloating}

Used to toggle setting fit function parameters in CR_RC::packPulseRefit().

Possible values: `0, 1`

#### isTrigger: int {#isTrigger}

If set to one the channel is excluded.

Possible values: `0, 1`

#### WindowStart: int {#WindowStart}

The bin to start the processor to look for pules, also acts as the stopping point when looking for false pulses.

#### WindowStop: int {#WindowStop}

The bin to stop the processor to look for pulses.

#### ThresholdWindowSize: double {#ThresholdWindowSize}

The size of the threshold window.

#### KernelSize: int {#KernelSize}

Kernel size for filtering algorithms.

#### l_delay: int {#l_delay}

Delay or lag.

#### constant_filter_fraction: double {#constant_filter_fraction}

Description NA.

#### SmoothingFactor: double {#SmoothingFactor}

Description NA.
