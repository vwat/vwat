# How our documentation works

# Overview

[Doxygen](https://www.doxygen.nl/manual) is used to generate our project documentation. It runs automatically via GitLab CI/CD for every commit made to the `master` branch, and the resulting documentation site is hosted using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works).

The Doxygen configuration can be modified by changing the `Doxyfile` in the project root. Currently, it targets all `.cxx`, `.hxx`, `.h`, and `.md` files.

# Code Comments

Doxygen is configured to pick up JavaDoc-formatted comments, which look like this:

    /**
     * The first sentence is extracted as a brief description (include the period!).
     * Anything after that is part of the more detailed class description.
     */
    class Class:
        // class body...

Refer to [this documentation](https://www.doxygen.nl/manual/docblocks.html#cppblock) for more information about the Doxygen syntax, which can be used to document everything from method parameters entire classes.

# Diagrams

Embed PlantUML directly into these Markdown documents using the following syntax:

    \ startuml
    !include ../docs/sample.puml
    \ enduml

**NOTE:** do not include a space between the backslash and the `start/enduml` directives -- this is just done here to show the raw syntax.

Doxygen will render the files to image format and place them directly on the page. Check out this sample:

\startuml
!include ../docs/sample.puml
\enduml

**NOTE:** Doxygen exports documentation files to a directory called `public/` under the project root. In order to successfully embed UML diagrams from your `.puml` files, you need to pass the `!include` directive a relative path from `public/` to the target file (refer to the example above)

# Generating Documentation

As a rule, the `master` branch should be the source of truth for the project documentation, but sometimes you may want to generate the documentation yourself.

## With Docker

The easiest way to generate the project documentation if you have Docker installed is by using the [`docgen`](../Dockerfile.docgen) container image included in this project:

    docker run --rm -v '<path to vwat repo on host>:/vwat' registry.gitlab.com/vwat/vwat/docgen -c 'make docs'

## Locally

You can run `make docs` to generate documentation locally, but you will need to ensure that the following dependencies are installed on your machine:

- [Doxygen](https://www.doxygen.nl/manual/install.html)
- [PlantUML](https://plantuml.com/faq-install)
- [Graphviz](https://graphviz.org/download/)

In order for Doxygen to find PlantUML on your system, set the environment variable `PLANTUML_JAR_PATH` to the absolute path of `plantuml.jar`.
